
This project uses the Plugin feature in the NSIWS to synchronize the MappingStore/Structure database with the DotStat data database.

### Requirement
The Dotstat-suite supports version 3.8.9 of RabbitMQ.

### Installation
#### Windows installtion
Options for installing the RabbitMQ software on Windows are the **windows installer** or using **Chocolatey**, for more information follow guide on RabbitMQ [website](https://www.rabbitmq.com/install-windows.html).
Other options are [Docker](https://registry.hub.docker.com/_/rabbitmq/) or [Kubernetes](https://www.rabbitmq.com/kubernetes/operator/quickstart-operator.html) setup. 
#### Setup
1. Rabbit MQ software setup  
> Either use Docker by running command `docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management`   
> or by follow installtion guide on https://www.rabbitmq.com/download.html     
> Once the installtion is completed, verify that the RabbitMq service is up and running by navigating to http://localhost:15672/, the username and password are guest/guest
> In this example the default configuration settings will be used
     
2. Dotstat plugin setup 
   i. Build and publish project by running command **dotnet publish -o out**
   ii. Copy all files in **out** folder to the **Plugin** folder located in the NSIWS root folder 
   iii. Copy **RabbitMq.json.sample** to the NSIWS config folder and remove **sample** suffix from the filename or use other methods for setting variables, such as environment variables. 
   iiii. Verify that the plugin is working by uploading a DSD using the NSIWS rest interface, this should create all database objects in the Dotstat data database.

3. Persist to not lose any data setting the queue the queue to persist and in case of using Docker, then map volumes as example below.

    rabbit-mq:
        image: rabbitmq:3.8.9-management
        container_name: rabbit-mq
        hostname: my-rabbit-mq
        ports:
          - "15672:15672"
          - "5672:5672"
        volumes:
          - "./rabbitmq/data/:/var/lib/rabbitmq/mnesia/"    #data 
          - "./logs:/var/log/rabbitmq/log"     #logs     
        networks:
          - my_network
```

<BR>Get all required artefacts and installation documentation from [Download](https://www.rabbitmq.com/download.html) 

### Configuration
The RabbitMQ software can run using the default configuration e.g. ports etc. More detailed configuration can be found [here](https://www.rabbitmq.com/install-windows.html#configure) (Windows)

#### Access control
By default the only user is *guest*, this user can only be used when accessing the RabbitMQ from localhost, read more on how to set access controls on [RabbitMQ website](https://www.rabbitmq.com/access-control.html).

## Producer
The **Producer** is the component that will push SDMX Dataflow artefacts that have been uploaded via the NSIWS to the RabbitMQ queue. 
 
### Installation
The **Producer** software is a plugin component to the NSIWS. To setup the plugin component, follow instructions
1. Clone repository https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer.git
2. Publish project DotStat.Transfer.Messaging.Producer
	- run *dotnet publish DotStat.Transfer.Messaging.Producer -o output* in project root folder

3. Copy following dlls to the Plugin folder in the NSIWS website
	- DotStat.Transfer.Messaging.Producer.dll
	- DotStat.Common.dll
    - RabbitMQ.Client.dll
4. Add configurations, either using a configuration file or by setting environment variables.

Description and example configuration

| Setting    |type| Description |
|------------|----|-------------|
|enabled|bool|Enables the messaging feature |
|hostName|string|the hostname of the RabbitMQ instance, default localhost |
|virtualHost|string|Virtual hosts provide logical grouping and separation of resources, [documentation](https://www.rabbitmq.com/vhosts.html) |
|port|string|By default, RabbitMQ will listen on port 5672 |
|userName|string|RabbitMQ user, *guest* by default |
|password|string|RabbitMQ password, *guest* by default |
|queueName|string|name of the queue used to indicate the Message broker routingKey |
|queueDurable|bool|Queues can be durable or transient. Metadata of a durable queue is stored on disk, while metadata of a transient queue is stored in memory when possible |
|deliveryMode|byte| sets if the queue item should be persistant in case of server shutdown, non-persistant(1) or persistant(2) |

```json
  "messaging": {
    "enabled": "true",
    "hostName": "localhost",
    "virtualHost": "/",
    "port": "5672",
    "userName": "user",
    "password": "password",
    "queueName": "dotstat",
    "queueDurable": "true",
    "deliveryMode": "2"
  }
```

## Consumer
The **Consumer** is a feature in the Transfer-service that subscribes to RabbitMQ events. 
If a new queue item is pushed to the queue then the **Consumer** application will be notified and pull the queue item. 
This queue item contains a list of dataflows that will be sent the Transfer-service init dataflow method.

### Installation
1. Add the configure settings using either a json file in the config folder or envirnoments variables with **enabled** setting set to true.


| Setting    |type| Description |
|------------|----|-------------|
|enabled|bool|Enables the messaging feature |
|hostName|string|the hostname of the RabbitMQ instance, default localhost |
|virtualHost|string|Virtual hosts provide logical grouping and separation of resources, [documentation](https://www.rabbitmq.com/vhosts.html) |
|port|string|By default, RabbitMQ will listen on port 5672 |
|userName|string|RabbitMQ user, *guest* by default |
|password|string|RabbitMQ password, *guest* by default |
|queueName|string|name of the queue used to indicate the Message broker routingKey |
|queueDurable|bool|Queues can be durable or transient. Metadata of a durable queue is stored on disk, while metadata of a transient queue is stored in memory when possible |
|deliveryMode|byte| sets if the queue item should be persistant in case of server shutdown, non-persistant(1) or persistant(2) |

```json
"messaging": {
    "enabled": "true",
    "hostName": "localhost", // when using Docker "host.docker.internal"
    "virtualHost": "/",
    "port": "5672",
    "userName": "user",
    "password": "password",
    "queueName": "dotstat-queue",
    "queueDurable": "true",
    "deliveryMode": "2"
```  



