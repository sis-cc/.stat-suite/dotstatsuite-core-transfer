﻿using System;
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Excel.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class SWApplicationException : DotStatException
    {
        public SWApplicationException() : base()
        {
        }

        public SWApplicationException(string message) : base(message)
        {
        }

        public SWApplicationException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}