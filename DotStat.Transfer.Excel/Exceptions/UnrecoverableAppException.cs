﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Excel.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class UnrecoverableAppException : DotStatException
    {
        public UnrecoverableAppException() : base()
        {
        }

        public UnrecoverableAppException(string message) : base(message)
        {
        }

        public UnrecoverableAppException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}