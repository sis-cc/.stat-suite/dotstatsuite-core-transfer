using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Text.RegularExpressions;
using DotStat.Common.Localization;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Util;

namespace DotStat.Transfer.Excel.Excel
{
    public abstract class ExpresssionBase
    {
        protected Func<T, TR> BuildFunc<T, TR>(string expr, bool catchParseException = true, string source = null)
            where T : ExpresssionBase
        {
            try
            {
                return DynamicExpression.ParseLambda<T, TR>(expr)
                    .Compile();
            }
            catch (ParseException ex)
            {
                if (!catchParseException)
                {
                    throw;
                }
                throw new UnrecoverableAppException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelExpressionParcingError),
                    ex.Message, expr,
                    source ?? ExpressionSource)
                );
            }
        }

        protected static CellReference[] InterpretExpressionResult(ExcelCellIterator owner, object res)
        {
            CellReference[] crl;
            var cr = res as CellReference;

            if (cr == null)
            {
                var cre = res as IEnumerable<CellReference>;

                if (cre != null)
                {
                    crl = cre.ToArray();
                }
                else
                {
                    var addr = res as string;

                    if (addr != null
                        && CellReference.CellReferenceRegex.IsMatch(addr))
                    {
                        cr = new CellReference(addr)
                        {
                            Owner = owner
                        };
                    }

                    crl = new[]
                    {
                        cr
                    };
                }
            }
            else
            {
                crl = new[]
                {
                    cr
                };
            }

            return crl.Where(r => r != null && r.Row > 0 && r.Column > 0)
                .ToArray();
        }

        internal readonly string ExpressionSource;
        protected internal string EvaluationSource { get; protected set; }
        private string _SheetName;
        private ExcelCellIterator _Owner;


        protected ExpresssionBase(string sheetName, string exprSource)
        {
            ExpressionSource = exprSource;
            _SheetName = sheetName;
        }

        // ReSharper disable once MemberCanBeInternal
        public virtual ExcelCellIterator Owner
        {
            get { return _Owner; }

            internal set
            {
                if (_Owner == value)
                {
                    return;
                }

                Reset();
                _Owner = value;
            }
        }

        /// <summary>
        /// This method clear any caches that are kept
        /// </summary>
        protected internal virtual void Reset()
        {
            //noop;
        }

        //*** here starts the methods for EDD expression functions

        // ReSharper disable MemberCanBeInternal
        // ReSharper disable UnusedMember.Global

        #region General properties
        public virtual int Row
        {
            get { return Owner.Current.Row; }
        }

        public virtual int Column
        {
            get { return Owner.Current.Column; }
        }

        public int StartRow
        {
            get { return Owner.StartRow; }
        }

        public int EndRow
        {
            get { return Owner.EndRow; }
        }

        public int StartColumn
        {
            get { return Owner.StartColumn; }
        }

        public int EndColumn
        {
            get { return Owner.EndColumn; }
        }

        public string SheetName
        {
            get { return _SheetName ?? (Owner != null ? Owner.SheetName : null); }
            protected set { _SheetName = value; }
        }

        public string WorkbookName
        {
            get
            {
                var wb = Owner.DataSource.WorkbookName;
                if (string.IsNullOrWhiteSpace(wb))
                {
                    return string.Empty;
                }
                var fi = wb.GetFileInfo();
                return fi.Name;
            }
        }

        public string WorkbookFullPath
        {
            get { return Owner.DataSource.WorkbookName; }
        }

        public string TemplateName
        {
            get { return WorkbookName; }
        }

        //public string DatasetCode
        //{
        //    get { return _Owner.Mapper.TargetDataset.Code; }
        //}

        public bool IsError
        {
            get { return !Owner.DataSource.IsCellValueValid(Row, Column, SheetName); }
        }

        public string Date
        {
            get { return DateTime.Now.ToString("yyyy-MM-dd"); }
        }

        public string Datetime
        {
            get { return DateTime.Now.ToString("yyyy-MM-dd HHmmss"); }
        }

        public virtual string CurrentMember
        {
            get { return string.Empty; }
        }

        public virtual string CurrentMemberCode
        {
            get { return string.Empty; }
        }

        public string FullCoordinates
        {
            get
            {
                return Coordinates == null
                    ? null
                    : Coordinates.Contains("!") ? Coordinates : (SheetName != null ? SheetName + "!" : "") + Coordinates;
            }
        }

        public virtual string Coordinates
        {
            get { return Owner.Current.Coordinates; }
        }

        public virtual string Value
        {
            get { return Owner.DataSource.GetCellValue(Coordinates, SheetName); }
        }

        public virtual string FormattedValue
        {
            get { return Owner.DataSource.GetFormattedCellValue(Coordinates, SheetName); }
        }

        public string Comments
        {
            get
            {
                if (Owner == null)
                {
                    throw new InvalidOperationException();
                }

                return Owner.DataSource.GetCellComment(Coordinates);
            }
            set
            {
                if (Owner == null)
                {
                    throw new InvalidOperationException();
                }

                Owner.DataSource.SetCellComment(value, Coordinates);
            }
        }

        #endregion

        #region Cell properties
        public CellReference CurrentCell
        {
            get
            {
                return new CellReference(Row, Column)
                {
                    Owner = Owner
                };
            }
        }

        public CellReference LeftCell
        {
            get
            {
                if (Column == 1)
                {
                    throw new InvalidCellReferenceException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelNoLeftCell),
                        Coordinates)
                    );
                }

                return new CellReference(Row, Column - 1)
                {
                    Owner = Owner
                };
            }
        }

        public CellReference RightCell
        {
            get
            {
                return new CellReference(Row, Column + 1)
                {
                    Owner = Owner
                };
            }
        }

        public CellReference CellAbove
        {
            get
            {
                if (Row == 1)
                {
                    throw new InvalidCellReferenceException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelNoAboveCell),
                        Coordinates)
                    );
                }

                return new CellReference(Row - 1, Column)
                {
                    Owner = Owner
                };
            }
        }

        public CellReference CellBelow
        {
            get
            {
                return new CellReference(Row + 1, Column)
                {
                    Owner = Owner
                };
            }
        }
        #endregion

        #region Functions

        public object FirstNonNull(object item1, params object[] items)
        {
            for (int ii = -1; ii < items.Length; ii++)
            {
                item1 = ii < 0 ? item1 : items[ii];

                if (item1 != null)
                {
                    var str = item1 as string;

                    if (str != null)
                    {
                        if (!string.IsNullOrEmpty(str))
                        {
                            return str;
                        }
                    }
                    else
                    {
                        return item1;
                    }
                }
            }

            return null;
        }

        public CellReference Cell(string column, int row, string sheetName = null)
        {
            if (row <= 0)
            {
                throw new InvalidCellReferenceException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelRowPositiveValue)
                );
            }
            if (string.IsNullOrWhiteSpace(column))
            {
                throw new InvalidCellReferenceException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelColumnLetterValue)
                );
            }
            var coord = (sheetName != null ? sheetName + "!" : "") + column + row;
            return new CellReference(coord)
            {
                Owner = Owner
            };
        }

        public CellReference Cell(int row, int column, string sheetName = null)
        {
            if (row <= 0)
            {
                //return CellReference.Null;
                throw new InvalidCellReferenceException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelRowPositiveValue)
                );
            }
            if (column <= 0)
            {
                throw new InvalidCellReferenceException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelColumnPositiveValue)
                );
            }
            return new CellReference(row, column, sheetName)
            {
                Owner = Owner
            };
        }

        public CellReference Cell(string cellCoordinates)
        {
            if (string.IsNullOrWhiteSpace(cellCoordinates))
            {
                throw new InvalidCellReferenceException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelCellNotation)
                );
            }
            return new CellReference(cellCoordinates)
            {
                Owner = Owner
            };
        }

        public IEnumerable<CellReference> FindCells(string range, string pattern, bool isRegex = false, bool nonMatcing = false)
        {
            var ptl = Owner.DataSource.FindCells(Owner.SheetName, range, pattern, isRegex, nonMatcing);
            return ptl.Select(pt => new CellReference(pt.Y, pt.X)
            {
                Owner = Owner
            });
        }

        public CellReference FindFirstCell(string range, string pattern, bool isRegex = false, bool nonMatcing = false)
        {
            var pt = Owner.DataSource.FindFirstCell(Owner.SheetName, range, pattern, isRegex, nonMatcing);
            if (pt.HasValue)
            {
                return new CellReference(pt.Value.Y, pt.Value.X)
                {
                    Owner = Owner
                };
            }
            throw new InvalidCellReferenceException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelFindFirstCellNoCellWithRangePattern),
                Owner.SheetName, range, pattern)
            );
        }

        public CellReference FindLastCell(string range, string pattern, bool isRegex = false, bool nonMatcing = false)
        {
            var pt = Owner.DataSource.FindLastCell(Owner.SheetName, range, pattern, isRegex, nonMatcing);
            if (pt.HasValue)
            {
                return new CellReference(pt.Value.Y, pt.Value.X)
                {
                    Owner = Owner
                };
            }
            throw new InvalidCellReferenceException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelFindLastCellNoCellWithRangePattern),
                Owner.SheetName, range, pattern)
            );
        }

        public CellReference FirstNonEmptyCell(string range)
        {
            var pt = Owner.DataSource.FindFirstCell(Owner.SheetName, range, "", false, true);
            if (pt.HasValue)
            {
                return new CellReference(pt.Value.Y, pt.Value.X)
                {
                    Owner = Owner
                };
            }
            throw new InvalidCellReferenceException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelFirstNonEmptyCell),
                Owner.SheetName, range)
            );
        }

        public CellReference LastNonEmptyCell(string range)
        {
            var pt = Owner.DataSource.FindLastCell(Owner.SheetName, range, "", false, true);
            if (pt.HasValue)
            {
                return new CellReference(pt.Value.Y, pt.Value.X)
                {
                    Owner = Owner
                };
            }
            throw new InvalidCellReferenceException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelLastNonEmptyCell),
                Owner.SheetName, range)
            );
        }

        public IEnumerable<int> FindRows(string range, string pattern = null, bool isRegex = false, bool nonMatching = false)
        {
            return Owner.DataSource.FindRows(Owner.SheetName, range, pattern, isRegex, nonMatching);
        }

        public IEnumerable<int> FindColumns(string range, string pattern = null, bool isRegex = false, bool nonMatching = false)
        {
            return Owner.DataSource.FindColumns(Owner.SheetName, range, pattern, isRegex, nonMatching);
        }

        public string TakeRight(string input, int count)
        {
            if (input == null)
            {
                return string.Empty;
            }
            if (input.Length < count)
            {
                return input;
            }
            return input.Substring(input.Length - count);
        }

        public string TakeLeft(string input, int count)
        {
            if (input == null)
            {
                return string.Empty;
            }
            if (input.Length <= count)
            {
                return input;
            }
            return input.Substring(0, count);
        }

        public string Trim(string input, bool trimInside = false)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }
            input = input.Trim();
            if (!trimInside)
            {
                return input;
            }
            int len;
            do
            {
                len = input.Length;
                input = input.Replace("  ", " ");
            } while (input.Length < len);
            return input;
        }

        public string LTrim(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }
            return input.TrimStart();
        }

        public string RTrim(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }
            return input.TrimEnd();
        }

        public string TakeCut(string input, int start, int count = -1)
        {
            if (input == null
                || input.Length < start
                || count == 0)
            {
                return string.Empty;
            }

            if (count < 0)
            {
                count = input.Length;
            }
            start--;

            if (start < 0)
            {
                count = count + start + 1;
                start = 0;
            }
            if (start + count >= input.Length)
            {
                return input.Substring(start);
            }

            return count < 0 ? input.Substring(start) : input.Substring(start, count);
        }

        public string Replace(string input, string search, string replacement)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(search))
            {
                return input;
            }

            if (replacement == null)
            {
                replacement = string.Empty;
            }
            return input.Replace(search, replacement);
        }

        public string TakeCutUntil(string input, int start, string until = null)
        {
            if (input == null
                || input.Length < start)
            {
                return string.Empty;
            }
            start--;

            if (start <= 0)
            {
                start = 0;
            }
            if (until == null)
            {
                return input.Substring(start);
            }
            var idx = input.IndexOf(until, start, StringComparison.InvariantCultureIgnoreCase);
            if (idx < start)
            {
                return input.Substring(start);
            }
            return input.Substring(start, idx - start);
        }

        public bool IsMatch(string pattern, string input, bool isRegex = true)
        {
            if (input == null && pattern == null)
            {
                return true;
            }
            if (pattern == null)
            {
                return false;
            }

            if (input == null)
            {
                input = string.Empty;
            }
            if (!isRegex)
            {
                return input.Trim()
                    .IsSameAs(pattern.Trim());
            }

            return Regex.IsMatch(input.Trim(), pattern.Trim(),
                RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
        }

        public bool TextContains(string pattern, string input)
        {
            if (input == null && pattern == null)
            {
                return true;
            }
            if (pattern == null)
            {
                return false;
            }

            if (input == null)
            {
                input = string.Empty;
            }
            return input.IndexOf(pattern, StringComparison.InvariantCultureIgnoreCase) >= 0;
        }

        public bool AreEqual(string s1, string s2)
        {
            return s1.IsSameAs(s2);
        }

        public bool IsEmpty(string input)
        {
            return string.IsNullOrWhiteSpace(input);
        }

        public bool IsNumeric(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }

            double num;
            return double.TryParse(input, NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out num);
        }

        public double ConvertToNumber(string input, string decimalSeparator = ".")
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return double.NaN;
            }
            if (string.IsNullOrEmpty(decimalSeparator))
            {
                decimalSeparator = ".";
            }
            var fdec = CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator[0];
            var res = new StringBuilder(input);
            var dec = decimalSeparator[0];
            var idx = input.Length - 1;
            var decCopied = false;
            for (var ii = idx; ii >= 0; ii--)
            {
                var ch = input[ii];
                if (((ch >= '0' && ch <= '9') || ch == '-'))
                {
                    res[idx--] = ch;
                }
                else if (!decCopied && ch == dec)
                {
                    res[idx--] = fdec;
                    decCopied = true;
                }
            }

            var output = res.ToString(idx + 1, input.Length - idx - 1);
            double num;
            if (double.TryParse(output, NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out num))
            {
                return num;
            }
            return double.NaN;
            //throw new ApplicationArgumentException("Text cannot be converted to a number: " + input );
        }

        public object If(bool condition, object trueResult, object falseResult)
        {
            return condition ? trueResult : falseResult;
        }

        public object Case(params object[] args)
        {
            if (args == null || args.Length == 0)
            {
                return null;
            }
            for (var ii = 0; ii < args.Length; ii += 2)
            {
                var eval = args[ii];
                if (args.Length > ii + 1 && eval is bool && (bool)eval)
                {
                    return args[ii + 1];
                }
            }
            return null;
        }

        

        //public bool CanResolveExternMember( string dimensionCode, string externMemberValue ) {
        //    return Owner.CoordDescriptor.Mapper.GetSWDimensionMember( dimensionCode, externMemberValue ) != null;
        //}

        //public List< string > MemberCodes( string dimCode ) {
        //    var dim = Owner.CoordDescriptor.Dataflow.FindDimension( dimCode, DbLanguage.Code );

        //    if( dim == null ) {
        //        throw new UnrecoverableAppException("The dataset {0} does not have a dimension with code {1}".F(Owner.CoordDescriptor.Dataflow.FullCode, dimCode));
        //    }

        //    return dim.Members.Select( mem => mem.Code )
        //              .ToList( );
        //}

        //public List< string > MemberNames( string dimCode ) {
        //    var dim = Owner.CoordDescriptor.Dataflow.FindDimension( dimCode, DbLanguage.Code );

        //    if( dim == null ) {
        //        throw new UnrecoverableAppException("The dataset {0} does not have a dimension with code {1}".F(Owner.CoordDescriptor.Dataflow.FullCode, dimCode));
        //    }

        //    return dim.Members.Select( mem => mem.EnglishName )
        //              .ToList( );
        //}

        //public List< string > MemberNamesFr( string dimCode ) {
        //    var dim = Owner.CoordDescriptor.Dataflow.FindDimension( dimCode, DbLanguage.Code );

        //    if( dim == null ) {
        //        throw new UnrecoverableAppException("The dataset {0} does not have a dimension with code {1}".F(Owner.CoordDescriptor.Dataflow.FullCode, dimCode));
        //    }

        //    return dim.Members.Select( mem => mem.FrenchName )
        //              .ToList( );
        //}

        #endregion

    }
}