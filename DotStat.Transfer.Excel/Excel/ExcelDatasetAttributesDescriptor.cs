﻿using System.Collections.Generic;
using System.Linq;

namespace DotStat.Transfer.Excel.Excel
{
    public class ExcelDatasetAttributesDescriptor
    {
        public readonly string Worksheet;
        public readonly IEnumerable<DatasetAttributeLookup> DatasetAttributeLookups;

        public ExcelDatasetAttributesDescriptor(string worksheet, IEnumerable<DatasetAttributeLookup> datasetAttributeLookups)
        {
            Worksheet = worksheet;
            DatasetAttributeLookups = datasetAttributeLookups ?? Enumerable.Empty<DatasetAttributeLookup>();
        }
    }

    public class DatasetAttributeLookup
    {
        public readonly string Code;
        public readonly string Coord;

        public DatasetAttributeLookup(string code, string coord)
        {
            Code = code;
            Coord = coord;
        }
    }
}