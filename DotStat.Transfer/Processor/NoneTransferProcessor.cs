﻿using DotStat.Domain;

namespace DotStat.Transfer.Processor
{
    public class NoneTransferProcessor : ITransferProcessor
    {
        public TransferContent Process(IImportReferenceableStructure sourceReferencedStructure, IImportReferenceableStructure destinationReferencedStructure, TransferContent transferContent)
        {
            return transferContent;
        }
    }
}
