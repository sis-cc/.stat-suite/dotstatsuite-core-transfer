﻿using DotStat.Common.Logger;
using DotStat.Domain;
using log4net.Core;
using DotStat.Transfer.Interface;

namespace DotStat.Transfer.Utils
{

    /// <summary>
    /// Execution status: Queued (0), InProgress (1), Completed (2), TimedOut (3), Canceled (4), default (any)
    /// </summary>
    public enum ExecutionStatus
    {
        Queued = TransactionStatus.Queued,
        InProgress = TransactionStatus.InProgress,
        Completed= TransactionStatus.Completed,
        TimedOut = TransactionStatus.TimedOut,
        Canceled = TransactionStatus.Canceled
    }

    /// <summary>
    /// Outcome of the transaction: Success (0), Warning (1), Error (2), None (3), default (any)
    /// </summary>
    public enum TransactionOutcome
    {
        /// <summary>
        /// Transaction completed successfully
        /// </summary>
        Success,
        /// <summary>
        /// Transaction completed with warnings
        /// </summary>
        Warning,
        /// <summary>
        /// Transaction failed
        /// </summary>
        Error,
        /// <summary>
        /// No information
        /// </summary>
        None
    }

    public class TransactionResult : ITransactionResult
    {

        public TransactionType TransactionType { get; set; }
        public int TransactionId { get; set; }
        public string SourceDataSpaceId { get; set; }
        public string DestinationDataSpaceId { get; set; }
        public string Aftefact { get; set; }
        public string User { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
        public string DataSource { get; set; }

        public LoggingEvent[] TransactionLogs => LogHelper.GetRecordedEvents(TransactionId, DestinationDataSpaceId);

    }
}