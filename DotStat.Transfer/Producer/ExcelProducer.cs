﻿using System.IO;
using System.Xml.Linq;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Common.Logger;
using DotStat.MappingStore;
using DotStat.Transfer.Excel.Excel;
using DotStat.Transfer.Excel.Reader;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using DotStat.Db;
using System;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Producer
{
    public class ExcelProducer : IProducer<ExcelToSqlTransferParam>
    {
        private readonly IMappingStoreDataAccess _dataAccess;
        private ExcelDataDescription _excelDataDescription;
        private EPPlusExcelDataSource _excelSource;

        public ExcelProducer(IMappingStoreDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public bool IsAuthorized(ExcelToSqlTransferParam transferParam, IImportReferenceableStructure referencedStructure)
        {
            //As the Source is Excel there is no need to check if the User can read Data.
            return true;
        }

        public Task<IImportReferenceableStructure> GetReferencedStructure(ExcelToSqlTransferParam transferParam, bool throwErrorIfNotFound=true)
        {
            _excelDataDescription = ExcelDataDescription.Build(
                1,
                string.Empty,
                string.Empty,
                XDocument.Load(new StreamReader(transferParam.EddFilePath)),
                _dataAccess,
                transferParam.DestinationDataspace?.Id);

            var dataFlow = _excelDataDescription.Dataflow;

            if (transferParam.DestinationDataspace != null)
                Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.DataflowLoaded,
                            transferParam.CultureInfo.TwoLetterISOLanguageName),
                        dataFlow.FullId,
                        transferParam.DestinationDataspace.Id));

            return Task.FromResult((IImportReferenceableStructure)dataFlow);
        }

        public Task<TransferContent> Process(ExcelToSqlTransferParam transferParam, IImportReferenceableStructure referencedStructure, CancellationToken cancellationToken)
        {
            if (referencedStructure is Dsd)
                throw new DotStatException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdReferenceNotSupportedExcel));

            if (!IsAuthorized(transferParam, referencedStructure))
            {
                throw new TransferUnauthorizedException();
            }

            _excelSource = new EPPlusExcelDataSource(transferParam.ExcelFilePath);

            var dimGroupSeriesKeyables = GetDimGroupSeriesFromExcelFile(referencedStructure as Dataflow);
            var observations = new SWCanonicalReader<IObservation>(_excelDataDescription.GetObservationCellIterator(_excelSource), referencedStructure as Dataflow).AsIEnumerable();
            var supportsTime = referencedStructure.Dsd.TimeDimension is not null;
            var nonTimeDimensions = referencedStructure.Dsd.Dimensions.Where(d => !d.Base.TimeDimension).ToList();
            var newObservations =new List<ObservationRow>();
            if (dimGroupSeriesKeyables != null)
            {
                foreach (var currentObservation in observations)
                {
                    var currentKeyableCoordinate = new HashSet<string>(currentObservation.SeriesKey.Key.Select(k => $"{k.Concept}:{k.Code}"));
                    var dimGroupSeriesAttributes = dimGroupSeriesKeyables
                        .Where(dimGroupSerie => dimGroupSerie.Key.IsSubsetOf(currentKeyableCoordinate))
                        .SelectMany(item => item.Value);

                    var allAttributes = new List<IKeyValue>(currentObservation.Attributes);
                    allAttributes.AddRange(dimGroupSeriesAttributes);

                    var isWildCarded = currentObservation.SeriesKey.Key.Any(kv => string.IsNullOrWhiteSpace(kv.Code)) || currentObservation.SeriesKey.Key.Any(kv => kv.Code.Equals(DbExtensions.DimensionWildCarded))
                        || nonTimeDimensions.Any(d => !currentObservation.SeriesKey.Key.Select(d => d.Concept).Contains(d.Code));//missing reported dimensions

                    if (supportsTime && (string.IsNullOrEmpty(currentObservation.ObsTime) || currentObservation.ObsTime.Equals(DbExtensions.DimensionWildCarded)))
                        isWildCarded = true;

                    //TODO: We could use a lighter object (Not IObservation) to improve performance
                    newObservations.Add(new ObservationRow(1, StagingRowActionEnum.Merge, new ObservationImpl(currentObservation.SeriesKey, currentObservation.ObsTime, currentObservation.ObservationValue, allAttributes, crossSectionValue:null), isWildCarded));
                };
            }
            else
            {
                foreach (var currentObservation in observations)
                {
                    var isWildCarded = currentObservation.SeriesKey.Key.Any(kv => string.IsNullOrWhiteSpace(kv.Code)) || currentObservation.SeriesKey.Key.Any(kv => kv.Code.Equals(DbExtensions.DimensionWildCarded))
                        || nonTimeDimensions.Any(d => !currentObservation.SeriesKey.Key.Select(d => d.Concept).Contains(d.Code));//missing reported dimensions

                    if (supportsTime && (string.IsNullOrEmpty(currentObservation.ObsTime) || currentObservation.ObsTime.Equals(DbExtensions.DimensionWildCarded)))
                        isWildCarded = true;

                    newObservations.Add(new ObservationRow(1, StagingRowActionEnum.Merge, currentObservation, isWildCarded)); 
                };
            }

            var dataSetAttributeRow = new DataSetAttributeRow(1, StagingRowActionEnum.Merge) 
            {
                Attributes = _excelDataDescription.DatasetAttributesDescriptor != null
                    ? new SWCanonicalReader<IKeyValue>(_excelDataDescription.GetDatasetAttributesIterator(_excelSource),
                        referencedStructure as Dataflow).AsIEnumerable().ToList()
                    : new List<IKeyValue>()
            };

            return Task.FromResult(new TransferContent
            {
                DataObservations = newObservations.ToAsyncEnumerable(),
                DatasetAttributes = new List<DataSetAttributeRow> { dataSetAttributeRow },
                ReportedComponents = new ReportedComponents
                {
                    DatasetAttributes = referencedStructure.Dsd.Attributes
                     .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet || a.Base.AttachmentLevel == AttributeAttachmentLevel.Null).ToList(),

                    //Attributes not attached to the time dimension
                    SeriesAttributesWithNoTimeDim = referencedStructure.Dsd.Attributes
                        .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                        .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                        .ToList(),

                    //Attributes attached to the time dimension
                    ObservationAttributes = referencedStructure.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                    || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList(),
                    Dimensions = referencedStructure.Dsd.Dimensions.ToList(),
                    TimeDimension = referencedStructure.Dsd.TimeDimension,
                    IsPrimaryMeasureReported = true
                }
            });
        }

        private Dictionary<HashSet<string>, List<IKeyValue>> GetDimGroupSeriesFromExcelFile(Dataflow dataflow)
        {
            var dimGroupSeriesKeyables = new Dictionary<HashSet<string>, List<IKeyValue>>();
            var seriesKeyables = new SWCanonicalReader<IKeyable>(_excelDataDescription.GetDimAttributesIterator(_excelSource), dataflow).AsIEnumerable();
            var enumerator = (seriesKeyables ?? Enumerable.Empty<IKeyable>()).GetEnumerator();

            while (enumerator.MoveNext())
            {
                var currentKeyable = enumerator.Current;
                var keyableCoordinate = new HashSet<string>(currentKeyable.Key.Select(k => $"{k.Concept}:{k.Code}"));
                dimGroupSeriesKeyables.Add(keyableCoordinate, currentKeyable.Attributes.ToList());                
                //
                // joining subsets to supersets might improve performance?
            }
            return dimGroupSeriesKeyables;
        }

        public void Dispose()
        {
            if (_excelSource != null)
            {
                _excelSource.Dispose();
            }
        }
    }
}