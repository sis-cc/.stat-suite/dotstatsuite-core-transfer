using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Exception;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Model;
using DotStat.Transfer.Param;
using Estat.Sdmxsource.Extension.Constant;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using System.Globalization;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using DotStat.Db.Util;

namespace DotStat.Transfer.Manager
{
    public class CommonManager : ICommonManager
    {
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;
        private readonly string _serviceId;

        public CommonManager(
            IAuthorizationManagement authorizationManagement,
            IMappingStoreDataAccess mappingStoreDataAccess,
            UnitOfWorkResolver unitOfWorkResolver,
            DotStatDbServiceResolver dotStatDbServiceResolver,
            IGeneralConfiguration configuration)
        {
            _authorizationManagement = authorizationManagement;
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbServiceResolver = dotStatDbServiceResolver;
            _serviceId = configuration.ServiceId;
        }

        public async Task<JObject> GetPITInfo(
            ITransferParam transferParam,
            Dataflow dataflow,
            CancellationToken cancellationToken)
        {

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            if (dataflow.Dsd == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowParameterWithoutDsd),
                    dataflow.FullId)
                );
            }

            if (!IsAuthorized(transferParam, dataflow)) 
            {
                throw new TransferUnauthorizedException();
            }

            await unitOfWork.ArtefactRepository.FillMeta(dataflow.Dsd, cancellationToken);

            if (dataflow.Dsd.PITVersion != null) 
            {
                if (dataflow.Dsd.PITReleaseDate == null || dataflow.Dsd.PITReleaseDate > DateTime.Now) 
                {
                    if (transferParam.PITReleaseDate != null) 
                    {
                        transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);

                        Log.SetTransactionId(transferParam.Id);
                        Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

                        try
                        {
                            var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                                transferParam.Id, dataflow.FullId, transferParam.Principal,
                                null, null, TransactionType.PITInfo, transferParam.TargetVersion, _serviceId, cancellationToken);

                            if (!await dotStatDbService.TryNewTransaction(transaction, dataflow, _mappingStoreDataAccess, cancellationToken, onlyLockTransaction: true))
                            {
                                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);

                                var dbId = transaction.ArtefactDbId ?? -1;
                                var isTransactionWithNoDsd = dataflow.Dsd is null;
                                var transactionInProgress = await unitOfWork.TransactionRepository.GetInProgressTransactions(transaction.Id, dbId, cancellationToken).FirstOrDefaultAsync(cancellationToken);

                                var errorMessage = isTransactionWithNoDsd
                                ? string.Format(
                                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                        .TransactionAbortedDueToConcurrentTransactionWithNoDsd),
                                    transaction.Id,
                                    transactionInProgress?.Id.ToString() ?? "unknown")
                                : string.Format(
                                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                        .TransactionAbortedDueToConcurrentTransaction),
                                    transaction.Id,
                                    transaction.ArtefactFullId,
                                    transactionInProgress?.Id.ToString() ?? "unknown");

                                throw new DotStatException(errorMessage);
                            }

                            if (transferParam.PITReleaseDate > DateTime.Now)
                            {
                                dataflow.Dsd.PITReleaseDate = transferParam.PITReleaseDate;

                                await unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(dataflow.Dsd, cancellationToken);
                            }
                            else
                            {
                                //Execute PIT Release if there is one pending 
                                var previousPITReleaseDate = dataflow.Dsd.PITReleaseDate;

                                await dotStatDbService.ApplyPITRelease(dataflow.Dsd, transferParam.PITRestorationAllowed, _mappingStoreDataAccess);

                                if (!dataflow.Dsd.LiveVersion.HasValue)
                                {
                                    throw new DotStatException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoValueSetForDsdLiveVersion));
                                }

                                var newDbTargetVersion = transferParam.TargetVersion == TargetVersion.Live
                                ? (DbTableVersion)dataflow.Dsd.LiveVersion
                                    : DbTableVersions.GetNewTableVersion((DbTableVersion)dataflow.Dsd.LiveVersion);

                                await unitOfWork.TransactionRepository.UpdateTableVersionOfTransaction(transferParam.Id, newDbTargetVersion, cancellationToken);

                                Log.Debug(string.Format(
                                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TargetVersionInDBChanged),
                                    transferParam.TargetVersion, newDbTargetVersion, previousPITReleaseDate.ToString())
                                );
                            }

                            await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);
                            Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TransactionSuccessful));
                        }
                        catch
                        {
                            // If there is an unexpected error do the cleanup transaction item before passing the exception
                            await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                            throw;
                        }
                    }

                    return new JObject
                    {
                        new JProperty("PITVersion", dataflow.Dsd.PITVersion?.ToString()),
                        new JProperty("PITReleaseDate", dataflow.Dsd.PITReleaseDate?.ToString("yyyy-MM-ddTHH:mm:ss.FFFzzz", CultureInfo.InvariantCulture)),
                        new JProperty("PITRestorationDate", dataflow.Dsd.PITRestorationDate?.ToString("yyyy-MM-ddTHH:mm:ss.FFFzzz", CultureInfo.InvariantCulture))
                    };
                }
            }

            //throw non existing PIT
            throw new TableVersionException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NonExistingPITVersion),
                dataflow.Dsd.FullId));
        }

        public async Task Rollback(ITransferParam transferParam, 
            Dataflow dataflow,
            CancellationToken cancellationToken)
        {
            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);
            try
            {
                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, dataflow.FullId,
                    transferParam.Principal, sourceDataSpace:null, dataSource:null,
                    TransactionType.Rollback, transferParam.TargetVersion, _serviceId , cancellationToken);

                transaction.FinalTargetVersion = transaction.RequestedTargetVersion;
                if (!await dotStatDbService.TryNewTransaction(transaction, dataflow, _mappingStoreDataAccess, cancellationToken))
                {
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                    return;
                }

                //POINT OF NO RETURN
                cancellationToken = CancellationToken.None;

                await dotStatDbService.Rollback(dataflow, _mappingStoreDataAccess, cancellationToken);
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);
                Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TransactionSuccessful));
            }
            catch (System.Exception)
            {
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                throw;
            }
        }

        public async Task Restore(
            TransferParam transferParam, 
            Transaction transaction,
            Dataflow dataflow,
            CancellationToken cancellationToken)
        {
            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);
            try
            {
                transaction.FinalTargetVersion = transaction.RequestedTargetVersion;
                if (!await dotStatDbService.TryNewTransaction(transaction, 
                    dataflow, _mappingStoreDataAccess, cancellationToken))
                {
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                    return;
                }

                //POINT OF NO RETURN
                cancellationToken = CancellationToken.None;

                await dotStatDbService.Restore(dataflow, _mappingStoreDataAccess, cancellationToken);
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);
                Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TransactionSuccessful));
            }
            catch (System.Exception)
            {
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transferParam"></param>
        /// <param name="targetDataflow"></param>
        /// <param name="managementRepository"></param>
        /// <param name="transactionRepository"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="offline">If authentication already been verified, e.g. using a message queue</param>
        /// <returns></returns>
        public async Task<bool> InitDataDbObjectsOfDataflow(
            TransferParam transferParam, 
            IDataflowMutableObject targetDataflow,
            CancellationToken cancellationToken,
            bool offline = false)
        {
            // Check of permission
            if (!offline && !_authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                targetDataflow.AgencyId,
                targetDataflow.Id,
                targetDataflow.Version,
                PermissionType.CanImportData))
            {
                // Not authorized to manage data of dataflow
                throw new TransferUnauthorizedException();
            }

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            // Check if dataflow exists in mappingstore database
            var dataflow = GetDataflow(transferParam.DestinationDataspace.Id, targetDataflow);

            if (dataflow == null)
            {
                // Dataflow not found in mappingstore database
                throw new ArtefactNotFoundException();
            }

            transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);

            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            try
            {
                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, dataflow.FullId, transferParam.Principal,
                    null, null, TransactionType.InitDataFlow, transferParam.TargetVersion, _serviceId, cancellationToken);

                if (!await dotStatDbService.TryNewTransaction(transaction, dataflow, _mappingStoreDataAccess, cancellationToken))
                {
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                    return false;
                }

                //Force to synchronize mappingsets
                // Closure of transaction. Includes (re-)calculation of actual content constraint
                await dotStatDbService.CloseTransaction(
                    transaction, 
                    dataflow, 
                    false, 
                    true, 
                    false,
                    _mappingStoreDataAccess, 
                    false, 
                    false, 
                    null, 
                    cancellationToken);

                return true;
            }
            catch
            {
                // If there is an unexpected error do the cleanup transaction item before passing the exception
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);

                throw;
            }
        }

        public async Task<bool> DeleteMappingSets(
            TransferParam transferParam, 
            IDataflowMutableObject targetDataflow,
            CancellationToken cancellationToken)
        {
            // Check of permission
            if (!_authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                targetDataflow.AgencyId,
                targetDataflow.Id,
                targetDataflow.Version,
                PermissionType.CanImportStructures))
            {
                // Not authorized to manage structures of dataflow
                throw new TransferUnauthorizedException();
            }

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            // Check if dataflow exists in mappingstore database
            var dataflow = GetDataflow(transferParam.DestinationDataspace.Id, targetDataflow, ResolveCrossReferences.DoNotResolve);

            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            // Check if dataflow exists in datastore database
            dataflow.DbId = await unitOfWork.ArtefactRepository.GetArtefactDbId(dataflow, cancellationToken);

            try
            {
                //Lock with transaction only when the dataflow exists in the data database
                if (dataflow.DbId != -1)
                {
                    if (dataflow.Dsd == null)
                    {
                        throw new DotStatException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowParameterWithoutDsd),
                            dataflow.FullId)
                        );
                    }

                    dataflow.Dsd.DbId = await unitOfWork.ArtefactRepository.GetArtefactDbId(dataflow.Dsd, cancellationToken);

                    var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                        transferParam.Id, 
                        dataflow.FullId, 
                        transferParam.Principal, 
                        sourceDataSpace:null, 
                        dataSource:null,
                        TransactionType.CleanupMappingSets, 
                        requestedTargetVersion:null, 
                        _serviceId,
                        cancellationToken
                    );

                    if (!await dotStatDbService.TryNewTransactionWithDsdId(
                            transaction,
                            dataflow.Dsd.DbId,
                            dataflow.DbId,
                            dataflow.Dsd.AgencyId, 
                            dataflow.Dsd.Code, 
                            dataflow.Dsd.Version, 
                            cancellationToken
                    ))
                    {
                        // Transaction creation attempt failed due to an ongoing concurrent transaction
                        throw new ConcurrentTransactionException();
                    }
                }

                //Delete mapping sets and data sets
                var deletedEntities =
                    _mappingStoreDataAccess.DeleteDataSetsAndMappingSets(transferParam.DestinationDataspace.Id, dataflow, deleteMsdObjectsOnly: false);

                //Close transaction only when the dataflow exists in the data database
                if (dataflow.DbId != -1)
                {
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);
                }

                return deletedEntities;
            }
            catch
            {
                //Close transaction only when the dataflow exists in the data database
                if (dataflow.DbId != -1)
                {
                    // If there is an unexpected error do the cleanup transaction item before passing the exception
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                }
                throw;
            }
        }

        public async Task<bool> CleanUpDsd(
            TransferParam transferParam, 
            IDataStructureMutableObject targetDsd,
            bool checkExistingDataStructure, 
            bool cleanupMsdOnly, 
            CancellationToken cancellationToken)
        {
            // Check of permission
            if (!_authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                targetDsd.AgencyId,
                targetDsd.Id,
                targetDsd.Version,
                PermissionType.CanDeleteStructuralMetadata))
            {
                // Not authorized to manage data of dataflow
                throw new TransferUnauthorizedException();
            }

            if (checkExistingDataStructure)
            {
                var mappingStoreObjects = _mappingStoreDataAccess.GetDsd(
                    transferParam.DestinationDataspace.Id,
                    targetDsd.AgencyId,
                    targetDsd.Id,
                    targetDsd.Version);

                if (mappingStoreObjects.DataStructures.Any())
                {
                    throw new DatastructureStillExistsException();
                }
            }

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            var dsdDbId = await unitOfWork.ArtefactRepository.GetArtefactDbId(targetDsd?.AgencyId, targetDsd?.Id, new SdmxVersion(targetDsd.Version), SDMXArtefactType.Dsd, cancellationToken);

            //If no such DSD
            if (dsdDbId == -1)
            {
                return false;
            }

            try
            {
                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, $"{targetDsd?.AgencyId}:{targetDsd?.Id}({targetDsd?.Version})", transferParam.Principal,
                    sourceDataSpace:null, dataSource:null,
                    TransactionType.CleanupDsd, requestedTargetVersion:null, _serviceId, cancellationToken);

                if (!await dotStatDbService.TryNewTransactionWithDsdId(transaction, dsdDbId, null, targetDsd.AgencyId, targetDsd.Id, new SdmxVersion(targetDsd.Version), cancellationToken))
                {
                    // Transaction creation attempt failed due to an ongoing concurrent transaction
                    throw new ConcurrentTransactionException();
                }

                var allDbComponents = (await unitOfWork.ComponentRepository.GetAllComponents(cancellationToken)).ToList();
                var allMsdDbComponents = (await unitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(cancellationToken)).ToList();

                await dotStatDbService.CleanUpDsd(dsdDbId, deleteMsdObjectsOnly: cleanupMsdOnly, _mappingStoreDataAccess, allDbComponents, allMsdDbComponents, cancellationToken);

                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);

                return true;
            }
            catch
            {
                // If there is an unexpected error do the cleanup transaction item before passing the exception
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);

                throw;
            }
        }

        public async Task<bool> CleanUpAllOrphans(TransferParam transferParam, Transaction transaction, CancellationToken cancellationToken)
        {
            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);
            try
            {
                if (!await dotStatDbService.TryNewTransactionWithNoDsd(transaction, cancellationToken))
                {
                    // Transaction creation attempt failed due to an ongoing concurrent transaction
                    throw new ConcurrentTransactionException();
                }

                var orphanDsdRemoved = await CleanUpOrphanedDsds(dotStatDbService, unitOfWork, transferParam.Principal, cancellationToken);

                var orphanCodelistsRemoved = await CleanUpOrphanedCodelists(dotStatDbService, unitOfWork, transferParam.Principal, cancellationToken);

                var orphanDataflowRemoved = await CleanUpOrphanedDataflows(dotStatDbService, unitOfWork, transferParam.Principal, cancellationToken);

                if (!orphanDsdRemoved && !orphanCodelistsRemoved && !orphanDataflowRemoved)
                    Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .NoOrphanDsdsToClean));
            }
            catch (System.Exception)
            {
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);

                throw;
            }

            await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, true);
            return true;
        }

        private async Task<bool> CleanUpOrphanedDsds(IDotStatDbService dotStatDbService, IUnitOfWork unitOfWork, DotStatPrincipal principal, CancellationToken cancellationToken)
        {
            var targetDsds = await unitOfWork.ArtefactRepository.GetListOfArtefacts(cancellationToken, "DSD");

            var orphansFound = false;

            var allDbComponents = (await unitOfWork.ComponentRepository.GetAllComponents(cancellationToken)).ToList();
            var allMsdDbComponents = (await unitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(cancellationToken)).ToList();

            foreach (var dsdItm in targetDsds)
            {
                try
                {
                    // Check of permission
                    if (!_authorizationManagement.IsAuthorized(
                        principal,
                        unitOfWork.DataSpace,
                        dsdItm.Agency,
                        dsdItm.Id,
                        dsdItm.Version,
                        PermissionType.CanDeleteStructuralMetadata))
                    {
                        // Not authorized to manage data of dataflow
                        Log.Notice(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .CleanUpErrorUnauthorized), dsdItm));

                        continue;
                    }

                    var mappingStoreObjects = _mappingStoreDataAccess.GetDsd(
                        unitOfWork.DataSpace,
                        dsdItm.Agency,
                        dsdItm.Id,
                        dsdItm.Version);

                    if (mappingStoreObjects.DataStructures.Any())
                        continue; // Do nothing if dsd still exists in structure db

                    var dsdDbId = await unitOfWork.ArtefactRepository.GetArtefactDbId(dsdItm.Agency, dsdItm.Id,
                        new SdmxVersion(dsdItm.Version), SDMXArtefactType.Dsd, cancellationToken);

                    if (dsdDbId == -1) continue;

                    await dotStatDbService.CleanUpDsd(dsdDbId, deleteMsdObjectsOnly: false, _mappingStoreDataAccess, allDbComponents, allMsdDbComponents, cancellationToken);

                    Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessful),
                        unitOfWork.DataSpace,
                        dsdItm
                    ));

                    orphansFound = true;
                }
                catch (System.Exception)
                {
                    Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpError),
                        dsdItm
                    ));
                }
            }

            return orphansFound;
        }

        private async Task<bool> CleanUpOrphanedCodelists(IDotStatDbService dotStatDbService, IUnitOfWork unitOfWork, DotStatPrincipal principal, CancellationToken cancellationToken)
        {
            // Obtain codelists from ARTEFACT table that have no relation to any components of any DSDs and MSDs in data database.
            var codelistArtefacts = await unitOfWork.ArtefactRepository.GetListOfCodelistsWithoutDsdAndMsd(cancellationToken);

            if (!codelistArtefacts.Any())
            {
                return false;
            }

            // Delete codelists from data database that have no relation to any DSD nor MSD
            foreach (var cl in codelistArtefacts)
            {
                try
                {
                    // Check of permission
                    if (!_authorizationManagement.IsAuthorized(
                        principal,
                        unitOfWork.DataSpace,
                        cl.Agency,
                        cl.Id,
                        cl.Version,
                        PermissionType.CanDeleteStructuralMetadata))
                    {
                        // Not authorized to manage data of dataflow
                        Log.Notice(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .CleanUpErrorUnauthorizedCodelist), cl));

                        continue;
                    }

                    if (await unitOfWork.ArtefactRepository.CleanUpCodelist(cl.DbId, cancellationToken))
                    {
                        Log.Notice(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessfulCodelist),
                            unitOfWork.DataSpace,
                            cl
                        ));
                    }
                }
                catch (System.Exception)
                {
                    Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorCodelist),
                        cl
                    ));
                }
            }

            return true;
        }

        private async Task<bool> CleanUpOrphanedDataflows(IDotStatDbService dotStatDbService, IUnitOfWork unitOfWork, DotStatPrincipal principal, CancellationToken cancellationToken)
        {
            // Obtain dataflows from ARTEFACT table that have no relation to any DSDs in the data database.
            var dataflowArtefacts = await unitOfWork.ArtefactRepository.GetListOfDataflowsWithoutDsd(cancellationToken);

            if (!dataflowArtefacts.Any())
            {
                return false;
            }

            // Delete dataflows from data database that have no relation to any DSD
            foreach (var df in dataflowArtefacts)
            {
                try
                {
                    // Check of permission
                    if (!_authorizationManagement.IsAuthorized(
                        principal,
                        unitOfWork.DataSpace,
                        df.Agency,
                        df.Id,
                        df.Version,
                        PermissionType.CanDeleteStructuralMetadata))
                    {
                        // Not authorized to manage data of dataflow
                        Log.Notice(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .CleanUpErrorUnauthorizedDataflow), df));

                        continue;
                    }

                    if (await unitOfWork.ArtefactRepository.CleanUpDataflow(df.DbId, cancellationToken))
                    {
                        Log.Notice(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessfulDataflow),
                            unitOfWork.DataSpace,
                            df
                        ));
                    }
                }
                catch (System.Exception)
                {
                    Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorDataflow),
                        df
                    ));
                }
            }

            return true;
        }


        public bool IsAuthorized(ITransferParam transferParam, Dataflow dataflow)
        {
            return _authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                dataflow.AgencyId,
                dataflow.Base.Id,
                dataflow.Version.ToString(),
                PermissionType.CanImportData
            );
        }

        public Dataflow GetDataflow(string sourceDataSpaceId, IDataflowMutableObject sourceDataflow, ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies)
        {
            var dataflow = _mappingStoreDataAccess.GetDataflow(
                sourceDataSpaceId,
                sourceDataflow.AgencyId,
                sourceDataflow.Id,
                sourceDataflow.Version,
                true,
                resolveCrossReferences);

            Log.Notice(
                string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowLoaded),
                    dataflow.FullId,
                    sourceDataSpaceId));

            return dataflow;
        }

        public async Task<bool?> TuneDsd(Dsd dsd, TransferParam transferParam, Transaction transaction, IndexTypeParamEnum indexType, CancellationToken cancellationToken)
        {
            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            try
            {
                if (!await dotStatDbService.TryNewTransactionWithDsdId(transaction, dsd.DbId,
                        null, dsd.AgencyId, dsd.Base.Id, dsd.Version, cancellationToken, onlyLockTransaction:false))
                {
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                    return null;
                }

                if (dsd.DataCompression == transferParam.DataCompression)
                {
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, true);

                    // Indexes already aligned with the requested ones.
                    Log.Warn(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .NewIndexTypeNotRequired),
                        indexType, dsd.FullId));
                    return false;
                }

                if (await dotStatDbService.ChangeDataCompression(dsd, transferParam.DataCompression,
                        _mappingStoreDataAccess,
                        cancellationToken))
                {
                    Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NewIndexTypeApplied),
                        indexType, dsd.FullId));
                }

                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, true);
            }
            catch
            {
                // If there is an unexpected error do the cleanup transaction item before passing the exception
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                throw;
            }

            return true;
        }


        public async Task<TuneInfo> GetTuneInfo(IDataStructureMutableObject dataStructure, TransferParam transferParam, CancellationToken cancellationToken)
        {
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            var dsdObj = _mappingStoreDataAccess.GetDsd(transferParam.DestinationDataspace?.Id, dataStructure.AgencyId,
                    dataStructure.Id, dataStructure.Version, false);

            //Dsd not found
            if (dsdObj == null)
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdNotFoundInDataspace),
                    dataStructure.AgencyId,
                    dataStructure.Id,
                    dataStructure.Version,
                    transferParam.DestinationDataspace?.Id));

            await unitOfWork.ArtefactRepository.FillMeta(dsdObj, cancellationToken);

            return new TuneInfo(dsdObj.DataCompression);
        }

        public async Task<bool?> OptimizeDsd(Dsd dsd, TransferParam transferParam, Transaction transaction, CancellationToken cancellationToken)
        {
            var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            try
            {
                if (!await dotStatDbService.TryNewTransactionWithDsdId(transaction, dsd.DbId,
                        null, dsd.AgencyId, dsd.Base.Id, dsd.Version, cancellationToken, onlyLockTransaction: false))
                {
                    await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                    return null;
                }

                var codeTranslator = new CodeTranslator(unitOfWork.CodeListRepository);
                await codeTranslator.FillDict(dsd, cancellationToken);

                await dotStatDbService.OptimizeDataForHighPerformanceReads(dsd, codeTranslator, cancellationToken);
                await dotStatDbService.OptimizeMetadataForHighPerformanceReads(dsd, codeTranslator, cancellationToken);
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, true);
            }
            catch
            {
                // If there is an unexpected error do the cleanup transaction item before passing the exception
                await unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transferParam.Id, false);
                throw;
            }

            return true;
        }

        public async Task<Dsd> GetOptimizeInfo(DataStructureMutableCore dataStructure, TransferParam transferParam, CancellationToken cancellationToken)
        {
            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace?.Id);

            var dsd = _mappingStoreDataAccess.GetDsd(transferParam.DestinationDataspace?.Id, dataStructure.AgencyId,
                dataStructure.Id, dataStructure.Version, false);

            //Dsd not found
            if (dsd == null)
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdNotFoundInDataspace),
                    dataStructure.AgencyId,
                    dataStructure.Id,
                    dataStructure.Version,
                    transferParam.DestinationDataspace?.Id));

            await unitOfWork.ArtefactRepository.FillMeta(dsd, cancellationToken);

            return dsd;
        }
    }
}
