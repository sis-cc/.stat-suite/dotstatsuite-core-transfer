﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Dto;
using DotStat.Domain;

namespace DotStat.Transfer.Param
{
    public interface ITransferParam
    {
        int Id { get; set; }
        DataspaceInternal SourceDataspace { get; set; }
        DataspaceInternal DestinationDataspace { get; set; }
        DotStatPrincipal Principal { get; set; }
        CultureInfo CultureInfo { get; set; }
        TargetVersion TargetVersion { get; set; }
        DateTime? PITReleaseDate { get; set; }
        Boolean PITRestorationAllowed { get; set; }
        string DataSource { get; set; }
        ValidationType ValidationType { get; set; }
        IList<string> FilesToDelete { get; set; }
        TransferType TransferType { get; set; }
        TransactionType TransactionType { get; set; }
        DataCompressionEnum DataCompression { get; set; }
        bool OptimizeForHighPerformanceReads { get; set; }
    }

    public interface ITransferParamWithFilePath : ITransferParam
    {
        string FilePath { get; set; }
        bool IsZipProcessed { get; set; }
    }

    public interface ITransferParamWithUrl : ITransferParamWithFilePath
    {
        Uri Url { get; set; }
        bool AuthenticateToRemoteUrl { get; set; }
        string Token { get; set; }
    }

    public class TransferParam : ITransferParam
    {
        public int Id { get; set; }
        public DataspaceInternal SourceDataspace { get; set; }
        public DataspaceInternal DestinationDataspace { get; set; }
        public DotStatPrincipal Principal { get; set; }
        public CultureInfo CultureInfo { get; set; }
        public TargetVersion TargetVersion { get; set; }
        public DateTime? PITReleaseDate { get; set; }
        public Boolean PITRestorationAllowed { get; set; }
        public string DataSource { get; set; }
        public ValidationType ValidationType { get; set; }
        public IList<string> FilesToDelete { get; set; } = new List<string>();
        public TransferType TransferType { get; set; } = TransferType.DataOnly;
        public TransactionType TransactionType { get; set; }

        public bool IsInited { get; set; }
        public ReportedComponents ReportedComponents { get; set; }
        public bool? IsXmlSource { get; set; }
        public DataCompressionEnum DataCompression { get; set; } = DataCompressionEnum.NONE;
        public SendEmailOptionsEnum SendEmail { get; set; } = SendEmailOptionsEnum.Always;
        public IImportReferenceableStructure ReferencedStructure { get; set; }
        public bool OptimizeForHighPerformanceReads { get; set; }
    }

    /// <summary>
    /// The type of validation to use during request: ImportWithBasicValidation (0), ImportWithFullValidation (1), FullValidationOnly (2).
    /// </summary>
    public enum ValidationType
    {
        /// <summary>
        /// Import/transfer data with full validation
        /// </summary>
        ImportWithBasicValidation = 0,
        /// <summary>
        /// Import/transfer data with full validation
        /// </summary>
        ImportWithFullValidation = 1,
        /// <summary>
        /// Full validation only (No data import/transfer).
        /// </summary>
        FullValidationOnly = 2,
    }

    /// <summary>
    /// The type of validation to use during import/transfer: ImportWithBasicValidation (0), ImportWithFullValidation (1) (default ImportWithBasicValidation (0).
    /// </summary>
    public enum ImportValidationType
    {
        /// <summary>
        /// Import/transfer data with full validation
        /// </summary>
        ImportWithFullValidation = ValidationType.ImportWithFullValidation,
        /// <summary>
        /// Import/transfer data with basic validation
        /// </summary>
        ImportWithBasicValidation = ValidationType.ImportWithBasicValidation,
    }
    /// <summary>
    /// Defines what resources will be included in the transfer: Option 0 - Transfer both data and metadata, Option 1 - Transfer only data, Option 2 - Transfer only metadata
    /// </summary>
    public enum TransferType
    {
        /// <summary>
        /// Transfer both data and metadata
        /// </summary>
        DataAndMetadata = 0,
        /// <summary>
        /// Transfer data only
        /// </summary>
        DataOnly = 1,
        /// <summary>
        /// Transfer metadata only
        /// </summary>
        MetadataOnly = 2
    }


    /// <summary>
    /// Send email options:  Always (0), Never (1), IfError (2), default (Always)
    /// </summary>
    public enum SendEmailOptionsEnum
    {
        Always = 0,
        Never = 1,
        IfError = 2
    }
}