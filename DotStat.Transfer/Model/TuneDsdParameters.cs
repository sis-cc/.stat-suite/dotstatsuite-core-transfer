﻿using DotStat.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotStat.Transfer.Model
{
    public class TuneInfo
    {
        public string IndexType;

        public TuneInfo(DataCompressionEnum dataCompression)
        {
            IndexType = GetIndexType(dataCompression).ToString();
        }

        private static IndexTypeEnum GetIndexType(DataCompressionEnum dataCompression)
        {
            return dataCompression switch
            {
                DataCompressionEnum.COLUMNSTORE => IndexTypeEnum.CLUSTERED_COLUMNSTORE,
                DataCompressionEnum.COLUMNSTORE_ARCHIVE => IndexTypeEnum.CLUSTERED_COLUMNSTORE_ARCHIVE,
                DataCompressionEnum.NONE => IndexTypeEnum.NONCLUSTERED_COLUMNSTORE,
                _ => throw new ArgumentOutOfRangeException(nameof(dataCompression), dataCompression, null)
            };
        }
    }

    public enum IndexTypeParamEnum
    {
        CLUSTERED_COLUMNSTORE = IndexTypeEnum.CLUSTERED_COLUMNSTORE,
        CLUSTERED_COLUMNSTORE_ARCHIVE = IndexTypeEnum.CLUSTERED_COLUMNSTORE_ARCHIVE
    }

    public enum IndexTypeEnum
    {
        NONCLUSTERED_COLUMNSTORE = 1,
        CLUSTERED_COLUMNSTORE = 2,
        CLUSTERED_COLUMNSTORE_ARCHIVE = 3
    }

}
