﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.Helpers;
using DotStat.DB;
using DotStat.MappingStore;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using MySqlConnector;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using DbType = DotStat.MappingStore.DbType;

namespace DotStatServices.Transfer.HealthCheck
{
    /// <summary>
    /// 
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class DbHealthCheck : IHealthCheck
    {
        private readonly IDataspaceConfiguration _configuration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public DbHealthCheck(IDataspaceConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            var result = true;
            var data = new Dictionary<string, object>();

            var supportedDbVersion = SupportedDatabaseVersion.DataDbVersion.ToString();

            foreach (var space in _configuration.SpacesInternal)
            {
                Dictionary<string, string> structureDb;
                Dictionary<string, string> dataDb;

                var type = DbTypeHelper.GetDbType(space.DbType, true);
                if (type == DbType.MariaDb)
                {
                    structureDb = await CheckMariaDbServerConnection(space.DotStatSuiteCoreStructDbConnectionString, cancellationToken);
                    dataDb = await CheckMariaDbServerConnection(space.DotStatSuiteCoreDataDbConnectionString, cancellationToken);
                }
                else
                {
                    structureDb = await CheckSqlServerConnection(space.DotStatSuiteCoreStructDbConnectionString, cancellationToken);
                    dataDb = await CheckSqlServerConnection(space.DotStatSuiteCoreDataDbConnectionString, cancellationToken);
                }

                var isDataDbVersionCompatible = SupportedDatabaseVersion.IsDataDbVersionSupported(dataDb?["version"]);
                var dbType = type.ToString();

                var useTablePartitioning = space.OptimizedForHighPerformanceReads && !string.IsNullOrEmpty(space.DefaultPartitioningScheme) && !string.IsNullOrEmpty(space.DefaultPartitioningColumn);

                data.Add(space.Id, new
                {
                    structureDb,
                    dataDb,
                    supportedDbVersion,
                    isDataDbVersionCompatible,
                    dbType,
                    optimizedForHighPerformanceReads = space.OptimizedForHighPerformanceReads,
                    useTablePartitioning
                });

                result &= structureDb != null && dataDb != null && isDataDbVersionCompatible;
            }

            return result 
                ? HealthCheckResult.Healthy(data:data) 
                : HealthCheckResult.Unhealthy(data:data);
        }

        private async Task<Dictionary<string, string>> CheckSqlServerConnection(string connectionString, CancellationToken cancellationToken)
        {
            try
            {
                var dict = new Dictionary<string, string>();
                var connString = new SqlConnectionStringBuilder(connectionString);
                var query = new StringBuilder("select 'version',[Version] from [dbo].[DB_VERSION]");
                var @params = new List<SqlParameter>();

                if (_configuration.ShowAdvanceDbHealthInfo)
                {
                    query.Append(@"
union all

select status + '_connections', cast(count(*) as varchar(5))
  from sys.dm_exec_sessions
  JOIN fn_my_permissions ( NULL, 'server' ) f ON f.permission_name = 'VIEW SERVER STATE'
 where [is_user_process] = 1 and db_name(database_id) = @dbName and [program_name]=@appName
 group by status

union all

select case [type_desc] when 'rows' then 'data' else 'logs' end, 
       cast(CAST(SUM(size * 8.0/1024) AS DECIMAL(15,2)) as varchar(20)) + ' MB'
  from sys.database_files
 group by [type_desc]
");

                    @params.Add(new SqlParameter("dbName", SqlDbType.VarChar)
                    {
                        Value = connString.InitialCatalog
                    });

                    @params.Add(new SqlParameter("appName", SqlDbType.VarChar)
                    {
                        Value = DbHelper.DbConnectionAppName
                    });
                }

                await using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync(cancellationToken);

                    await using (var cmd = connection.CreateCommand())
                    {
                        cmd.CommandText = query.ToString();
                        cmd.Parameters.AddRange(@params.ToArray());

                        await using (var reader = await cmd.ExecuteReaderAsync(cancellationToken))
                        {
                            while (await reader.ReadAsync(cancellationToken))
                            {
                                dict.Add(reader.GetString(0), reader.GetString(1));
                            }
                        }
                    }
                }

                return dict;
            }
            catch
            {
                return null;
            }
        }
        
        private async Task<Dictionary<string, string>> CheckMariaDbServerConnection(string connectionString, CancellationToken cancellationToken)
        {
            try
            {
                var dict = new Dictionary<string, string>();
                var connString = new MySqlConnectionStringBuilder(connectionString);
                var query = new StringBuilder("select 'version',`Version` from DB_VERSION");
                var @params = new List<MySqlParameter>();

                if (_configuration.ShowAdvanceDbHealthInfo)
                {
                    query.Append(@"
union all

SELECT 'status_connections', CAST(COUNT(1) AS CHAR(5))
FROM information_schema.PROCESSLIST
WHERE DB = @dbName;
");

                    @params.Add(new MySqlParameter("dbName", MySqlDbType.VarChar)
                    {
                        Value = connString.Database
                    });
                }

                await using (var connection = new MySqlConnection(connectionString))
                {
                    await connection.OpenAsync(cancellationToken);

                    await using (var cmd = connection.CreateCommand())
                    {
                        cmd.CommandText = query.ToString();
                        cmd.Parameters.AddRange(@params.ToArray());

                        await using (var reader = await cmd.ExecuteReaderAsync(cancellationToken))
                        {
                            while (await reader.ReadAsync(cancellationToken))
                            {
                                dict.Add(reader.GetString(0), reader.GetString(1));
                            }
                        }
                    }
                }

                return dict;
            }
            catch
            {
                return null;
            }
        }

    }
}
