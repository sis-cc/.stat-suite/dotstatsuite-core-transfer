﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.MappingStore;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace DotStatServices.Transfer.HealthCheck
{
    /// <summary>
    /// 
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ServiceHealthCheck : IHealthCheck
    {
        private readonly BaseConfiguration _configuration;
        private readonly IAuthConfiguration _authConfiguration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public ServiceHealthCheck(BaseConfiguration configuration, IAuthConfiguration authConfiguration)
        {
            _configuration = configuration;
            _authConfiguration = authConfiguration;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            var configurationOk = _configuration != null && _configuration.SpacesInternal.Any();
            if (!configurationOk)
            {
                return HealthCheckResult.Unhealthy();
            }

            foreach (var space in _configuration.SpacesInternal)
            {
                if (!IsArchiveSpaceUsingMariaDb(space)) continue;
                var error = new Dictionary<string, object>()
                {
                    {
                        "error", string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .NotSupportedArchiveAndMariaDb), $"'{space.Id}'")
                    }
                };
                return HealthCheckResult.Unhealthy(data: error);
            }

            HealthCheckResult.Healthy(data: WebInfo());
            return _configuration != null && _configuration.SpacesInternal.Any()
                ? HealthCheckResult.Healthy(data: WebInfo())
                : HealthCheckResult.Unhealthy();
        }

        private Dictionary<string, object> WebInfo()
        {
            return new Dictionary<string, object>()
            {
                {"version", GetInformationalVersion(Assembly.GetExecutingAssembly())},
                {"auth_enabled", _authConfiguration.Enabled},
                {"tz", System.TimeZoneInfo.Local.DisplayName},
                {"time", System.DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")},
                {"cpu", Environment.ProcessorCount}
            };
        }

        private string GetInformationalVersion(Assembly assembly)
        {
            return FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;
        }

        private bool IsArchiveSpaceUsingMariaDb(DataspaceInternal dataSpace)
        {
            var type = DbTypeHelper.GetDbType(dataSpace.DbType, true);
            return type == DbType.MariaDb && dataSpace.Archive;
        }
    }
}
