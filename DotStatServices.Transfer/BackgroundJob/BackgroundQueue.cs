﻿using DotStat.Transfer.Utils;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace DotStatServices.Transfer.BackgroundJob
{
    /// <summary>
    /// Class to queue background tasks, registered as a singleton in startup
    /// Todo: consider if we need separate queues for import and transfer, if yes this class can be made generic with separate instances for different queues.
    /// </summary>
    public class BackgroundQueue
    {
        private readonly Action<Exception> _onException;
        private readonly ConcurrentQueue<TransactionQueueItem> Queue = new ConcurrentQueue<TransactionQueueItem>();
        private int _runningTasks;
        private long _takenTasks;
        internal readonly int MaxConcurrentCount;
        internal readonly int MillisecondsToWaitBeforePickingUpTask;

        internal long TakenTasks => _takenTasks;
        internal int TaskInQueue => Queue.Count;
        internal int RunningTasks => Interlocked.CompareExchange(ref _runningTasks, 0, 0);

        // user for mocking in unit test
        public BackgroundQueue() : this(null, 10, 1000)
        {}

        public BackgroundQueue(Action<Exception> onException, int maxConcurrentCount, int millisecondsToWaitBeforePickingUpTask)
        {
            if (millisecondsToWaitBeforePickingUpTask < 500)
                throw new ArgumentException("< 500 Milliseconds will eat the CPU", nameof(millisecondsToWaitBeforePickingUpTask));

            if (maxConcurrentCount < 1)
                throw new ArgumentException("maxConcurrentCount must be at least 1", nameof(maxConcurrentCount));

            _onException = onException ?? (exception => { });
            MaxConcurrentCount = maxConcurrentCount;
            MillisecondsToWaitBeforePickingUpTask = millisecondsToWaitBeforePickingUpTask;
        }

        public virtual void Enqueue(TransactionQueueItem queueItem)
        {
            Queue.Enqueue(queueItem);
        }
        
        internal void ParallelDequeue(CancellationToken cancellationToken)
        {
            var maxDegreeOfParallelism = MaxConcurrentCount - RunningTasks;

            if (maxDegreeOfParallelism <= 0)
                return;

            var parallelOptions = new ParallelOptions
            {
                MaxDegreeOfParallelism = maxDegreeOfParallelism
            };
            
            Parallel.For(0, TaskInQueue, parallelOptions, async (i) =>
            {
                if (Queue.TryDequeue(out var deQueuedItem))
                {
                    Interlocked.Increment(ref _takenTasks);
                    Interlocked.Increment(ref _runningTasks);

                    try
                    {
                        //Check if nothing is blocking this request (mangement requests or other requests targeting the same dsd)
                        if (!await deQueuedItem.CanBeProcessed(cancellationToken))
                        {
                            //Delay the requeue of this request to allow the blocking request(s) to be completed.
                            await deQueuedItem.DelayRequeue(cancellationToken);

                            //Requeue request 
                            Queue.Enqueue(deQueuedItem);
                            return;
                        }

                        //No Blocking transactions - The request can be processed
                        using var internalCancellationToken = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
                        
                        var mainTask = deQueuedItem.MainTask(internalCancellationToken.Token);
                        var notifyInProgressTask = deQueuedItem.NotifyInProgressTask(internalCancellationToken.Token);

                        await Task.WhenAny(mainTask, notifyInProgressTask);
                        internalCancellationToken.Cancel();
                    }
                    catch (Exception e)
                    {
                        _onException(e);
                    }
                    finally
                    {
                        Interlocked.Decrement(ref _runningTasks);
                    }
                }
            });
        }

        internal async Task CloseAllQueuedTransactions()
        {
            foreach (var queueItem in Queue.ToArray())
            {
                queueItem.CallbackAction.Invoke();
            }
            await Task.CompletedTask;
        }
    }
}
