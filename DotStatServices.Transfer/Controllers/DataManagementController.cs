using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using Estat.Sdmxsource.Extension.Constant;
using DotStatServices.Transfer.BackgroundJob;
using System.Threading.Tasks;
using DotStat.Common.Exceptions;
using DotStat.MappingStore.Exception;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Utils;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.MappingStore;
using static DotStat.Common.Localization.LocalizationRepository;
using DotStat.Transfer.Model;
using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using System.Data;
using System.Runtime;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// CleanUp controller
    /// </summary>
    [ApiVersion("1.2")]
    [ApiVersion("2")]
    [ApiVersion("3")]
    [ApiController]
    [Route("{apiVersion:apiVersion}/")]
    public class DataManagementController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly BaseConfiguration _configuration;
        private readonly CommonManager _commonManager;
        private readonly BackgroundQueue _backgroundQueue;
        private readonly IMailService _mailService;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;

        /// <summary>        
        /// Constructor for this controller filled by DryIoc
        /// </summary>
        public DataManagementController(
            IHttpContextAccessor contextAccessor,
            IAuthConfiguration authConfiguration,
            IAuthorizationManagement authorizationManagement,
            IMappingStoreDataAccess mappingStoreDataAccess,
            BaseConfiguration configuration,
            CommonManager commonManager,
            IMailService mailService,
            BackgroundQueue backgroundQueue,
            UnitOfWorkResolver unitOfWorkResolver,
            DotStatDbServiceResolver dotStatDbServiceResolver)

        {
            _contextAccessor = contextAccessor;
            _authConfiguration = authConfiguration;
            _authorizationManagement = authorizationManagement;
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _configuration = configuration;
            _commonManager = commonManager;
            _mailService = mailService;
            _backgroundQueue = backgroundQueue;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbServiceResolver = dotStatDbServiceResolver;
        }

        /// <summary>
        /// Initialise or repair DB objects of a dataflow in a given datastore DB (creates all Data DB objects - or repair missing/broken structures - required for the dataflow usage for extractions, and generates an empty actual content constraint for the dataflow)
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace where to initialize the db objects. <br></br> Example: <code>design</code></param>
        /// <param name="dataflow">The SDMX ID of the dataflow.<br></br> Format: 'AGENCYID:DATAFLOWID(VERSION)'. <br></br> Example: <code>OECD:DF_MEI(1.0)</code></param>
        /// <remarks>
        /// Example: Initialise the database objects of the dataflow 'OECD:MEI(1.0)' in the 'design' dataspace
        /// 
        ///     POST /init/dataflow
        ///     {
        ///        "dataspace": "design",
        ///        "dataflow": "OECD:DF_MEI(1.0)"
        ///     }
        ///     
        /// </remarks>
        /// <response code="201">Initialisation or repair successfully completed</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="401">Not authorized.</response>
        /// <response code="409">Conflict. There is an ongoing transaction targeting the DSD of the dataflow</response>
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [MapToApiVersion("3")]
        [Microsoft.AspNetCore.Mvc.Route("init/dataflow")]
        public async Task<ActionResult<OperationResult>> InitDatabaseObjectsOfDataflow(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dataflow)
        {
            var cancellationToken = CancellationToken.None;
            var destinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode);
            var transferParam = new TransferParam()
            {
                TransactionType = TransactionType.InitDataFlow,
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = destinationDataspace,
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null,
                DataCompression = destinationDataspace.GetDataCompression()
            };

            try
            {
                if (await _commonManager.InitDataDbObjectsOfDataflow(transferParam, dataflow.GetDataflow(_configuration.DefaultLanguageCode), cancellationToken))
                {
                    return Created(
                        string.Empty,
                        GetResult(
                            true,
                            dataflow,
                            Localization.ResourceId.InitDBObjectsOfDataflowSuccess,
                            transferParam.Id,
                            dataspace));
                }

                return new ConflictObjectResult(
                    GetResult(
                        false,
                        dataflow,
                        Localization.ResourceId.InitDBObjectsOfDataflowFail,
                        transferParam.Id,
                        dataspace));

            }
            catch (TransferUnauthorizedException)
            {
                return new ForbidResult();
            }
            catch (ExternalDataflowException)
            {
                return new BadRequestObjectResult(
                    GetResult(
                        false,
                        dataflow,
                        Localization.ResourceId.InitExternalDataflow,
                        transferParam.Id,
                        dataspace));
            }
            catch (DotStatException exception)
            {
                Log.Error(exception);
                throw;
            }
            catch (Exception exception)
            {
                Log.Error(string.Format(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurred, transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                Log.Fatal(exception);
                throw;
            }
        }

        private static OperationResult GetResult(
            bool isSuccess,
            string artefactFullId,
            Localization.ResourceId resourceId,
            int transactionId, 
            string dataspaceId)
        {
            return new OperationResult(
                isSuccess,
                string.Format(GetLocalisedResource(resourceId), artefactFullId)
            )
            {
                Detail = LogHelper.GetRecordedEvents(transactionId, dataspaceId).Select(o=>new
                {
                    Log = o.Level.DisplayName,
                    Message = o.RenderedMessage
                })
            };
        }

        /// <summary>
        /// Create Mappingsets of all dataflows in the mappingstore database for a given dataspace
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace where to initialise the mappingsets. <br></br> Example: <code>design</code></param>
        /// <param name="sendEmail"> Optional - Indicate when the service should send email. <br></br>Possible values: Always (0), Never (1), IfError (2), default (Always)</param>
        /// <remarks>
        /// Example: Create mappingsets for all dataflows found in the 'design' dataspace
        /// 
        ///     POST /init/allMappingsets
        ///     {
        ///        "dataspace": "design"
        ///     }
        ///     
        /// </remarks>
        /// <response code="200">Successfully completed</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="401">Not authorized.</response>
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [MapToApiVersion("3")]
        [Route("init/allMappingsets")]
        public async Task<ActionResult<OperationResult>> InitAllMappingsets(
            [FromForm, Required] string dataspace,
            [FromForm] SendEmailOptionsEnum sendEmail = SendEmailOptionsEnum.Always
        )
        {
            var cancellationToken = CancellationToken.None;
            var destinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode);

            var transferParam = new TransferParam
            {
                TransactionType = TransactionType.InitAllMappingSets,
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = destinationDataspace,
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User,
                _authConfiguration.ClaimsMapping)
            };

            return await CreateAllMappingsets(transferParam, sendEmail, cancellationToken);
        }

        /// <summary>
        /// Apply a specific type of index to tune the performance of the DSD tables storing the data and referential metadata for a given dataspace
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace where to tune the DSD. <br></br> Example: <code>design</code></param>
        /// <param name="dsd">The SDMX ID of the DSD to be tuned <br></br> Format: 'AGENCYID:DSD ID(VERSION)' <br></br> Example: <code>OECD:MEI(1.0)</code></param>
        /// <param name="indexType">The type of index to be applied to the DSD:  <br></br> indexType Options <br></br><b> - CLUSTERED_COLUMNSTORE (2)</b> - Improves performance for data extractions for DSDs with millions of observations.<br></br><b>- CLUSTERED_COLUMNSTORE_ARCHIVE (3)</b> - High compression of data to save disk space; Makes data imports and exports slower.</param>
        /// <remarks>
        /// Example: Tune the DSD 'OECD:MEI(1.0)' with index type 'CLUSTERED_COLUMNSTORE' in the 'design' dataspace
        /// 
        ///     POST /tune/dsd
        ///     {
        ///        "dataspace": "design",
        ///        "dsd": "OECD:MEI(1.0)",
        ///        "indexType": 2
        ///     }
        ///     
        /// </remarks>
        /// <response code="200">Successfully completed</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="403">Not authorized.</response>
        /// <response code="404">Not found.</response>
        /// <response code="501">Not Implemented.</response>
        [HttpPost]
        [MapToApiVersion("3")]
        [Route("tune/dsd")]
        public async Task<ActionResult<OperationResult>> TuneDsd(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dsd,
            [FromForm, Required] IndexTypeParamEnum indexType
        )
        {
            var cancellationToken = CancellationToken.None;
            var lang = _configuration.DefaultLanguageCode?? Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;

            var destinationDataSpace = dataspace.GetSpaceInternal(_configuration, lang);
            if (!destinationDataSpace.CheckTuneDsdIsSupported())
            {
                var message = GetLocalisedResource(Localization.ResourceId.NotSupportedTuneDsdAndMariaDb);
                var operationResult = new OperationResult(false, message);
                return StatusCode(StatusCodes.Status501NotImplemented, operationResult);
            }
            
            var dataStructure = dsd.GetDsd(lang);
            var transferParam = new TransferParam()
            {
                Principal = new DotStatPrincipal(_contextAccessor?.HttpContext?.User, _authConfiguration.ClaimsMapping),
                TransactionType = TransactionType.TuneDsd,
                CultureInfo = CultureInfo.GetCultureInfo(lang),
                DestinationDataspace = destinationDataSpace,
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null,
                DataCompression = indexType.GetCompression()
            };

            // Check of permission
            if (!CanManageTuneInfo(transferParam, dataStructure))
            {
                // Not authorized to manage indexes of dsd
                return new ForbidResult();
            }

            var dsdObj = _mappingStoreDataAccess.GetDsd(destinationDataSpace?.Id, dataStructure.AgencyId,
                dataStructure.Id, dataStructure.Version, false);
            
            if (dsdObj == null)
            {
                //Dsd not found
                return new ActionResult<OperationResult>(new NotFoundObjectResult(string.Format(
                    GetLocalisedResource(Localization.ResourceId.DsdNotFoundInDataspace, lang),
                    dataStructure.AgencyId, dataStructure.Id, dataStructure.Version, destinationDataSpace?.Id)));
            }

            try
            {
                var dotStatDbService = _dotStatDbServiceResolver(transferParam?.DestinationDataspace?.Id);
                var unitOfWork = _unitOfWorkResolver(transferParam?.DestinationDataspace?.Id);

                transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);
                transferParam.TransactionType = TransactionType.TuneDsd;

                var transactionResult = new TransactionResult
                {
                    TransactionType = transferParam.TransactionType,
                    TransactionId = transferParam.Id,
                    DestinationDataSpaceId = transferParam.DestinationDataspace?.Id,
                    Aftefact = dsdObj.FullId,
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.Queued
                };

                await dotStatDbService.FillIdsFromDisseminationDb(dsdObj, cancellationToken);

                LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);

                var message = string.Format(
                    GetLocalisedResource(Localization.ResourceId.SubmissionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id);
                Log.Notice(message);

                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, dsdObj.FullId, transferParam.Principal,
                    transferParam.DestinationDataspace?.Id, string.Empty, 
                    transferParam.TransactionType, transferParam.TargetVersion,
                    _configuration.ServiceId, cancellationToken);
                
                //Function with the main steps to execute the request
                async Task MainTask(CancellationToken backgroundCancellationToken)
                {
                    using var internalCancellationToken = backgroundCancellationToken.SetBackgroundTaskCompletionSteps(dotStatDbService, transferParam, cancellationToken);

                    try
                    {
                        await _commonManager.TuneDsd(dsdObj, transferParam, transaction, indexType, cancellationToken);
                    }
                    catch (DotStatException exception)
                    {
                        Log.Warn(GetLocalisedResource(Localization.ResourceId.ErrorChangingIndexType, transferParam.CultureInfo.TwoLetterISOLanguageName));
                        Log.Error(exception);
                    }
                    catch (Exception exception)
                    {
                        if (exception is not TaskCanceledException && !internalCancellationToken.Token.IsCancellationRequested)
                        {
                            Log.Error(string.Format(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurred, transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                            Log.Fatal(exception);
                        }
                    }
                    finally
                    {
                        transactionResult.TransactionStatus = await dotStatDbService.GetFinalTransactionStatus(transferParam.Id, CancellationToken.None);

                        await _mailService.SendMail(
                            transactionResult,
                            transferParam.CultureInfo.TwoLetterISOLanguageName,
                            _authorizationManagement.IsAuthorized(transferParam.Principal, PermissionType.AdminRole), SendEmailOptionsEnum.Always
                        );
                    }

                    await Task.CompletedTask;
                }

                //Delegate to check if this request can begin to process, or if it should be placed back to the queue.
                async Task<bool> CanBeProcessed(CancellationToken backgroundCancellationToken)
                {
                    Log.SetTransactionId(transferParam.Id);
                    Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
                    return await dotStatDbService.TryNewTransactionWithDsdId(transaction, dsdObj.DbId,
                        null, dsdObj.AgencyId, dsdObj.Base.Id, dsdObj.Version, backgroundCancellationToken, onlyLockTransaction: true);
                }

                //Function to execute to notify/log that the request is still in progress.
                async Task NotifyInProgressTask(CancellationToken backgroundCancellationToken) => await backgroundCancellationToken.SetStillProcessingLogging(transferParam);

                //Action to execute when the application is shutting down
                async void CallBackAction() => await unitOfWork.TransactionRepository.MarkTransactionAsCanceled(transferParam.Id);

                _backgroundQueue.Enqueue(new TransactionQueueItem(CanBeProcessed, MainTask, NotifyInProgressTask, CallBackAction));

                return new OperationResult(true, message);
            }
            catch (Exception exception)
            {
                if (transferParam.Id == 0)
                {
                    Log.Error(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurredNoId, transferParam.CultureInfo.TwoLetterISOLanguageName));
                }
                else
                {
                    Log.Error(string.Format(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurred, transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                }
                Log.Fatal(exception);
                throw;
            }
        }

        /// <summary>
        /// Get the indexType information currently applied for the DSD tables storing the data and referential metadata for a given dataspace
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace where to tune the DSD. <br></br> Example: <code>design</code></param>
        /// <param name="dsd">The SDMX ID of the DSD to be tuned <br></br> Format: 'AGENCYID:DSD ID(VERSION)' <br></br> Example: <code>OECD:MEI(1.0)</code></param>
        /// <remarks>
        /// Example: Get the index type information for the DSD 'OECD:MEI(1.0)', found in the 'design' dataspace
        /// 
        ///     POST /tune/info
        ///     {
        ///        "dataspace": "design",
        ///        "dsd": "OECD:MEI(1.0)"
        ///     }
        /// </remarks>
        /// <response code="200">Successfully completed </response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="403">Not authorized.</response>
        /// <response code="404">Not found.</response>
        [HttpPost]
        [MapToApiVersion("3")]
        [Route("tune/info")]
        public async Task<ActionResult<OperationResult>> TuneInfo(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dsd
        )
        {
            var lang = _configuration.DefaultLanguageCode ?? Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
            var destinationDataSpace = dataspace.GetSpaceInternal(_configuration, lang);
            var dataStructure = dsd.GetDsd(lang);
            var cancellationToken = CancellationToken.None;
            var transferParam = new TransferParam
            {
                DestinationDataspace = destinationDataSpace,
                Principal = new DotStatPrincipal(_contextAccessor?.HttpContext?.User, _authConfiguration.ClaimsMapping)
            };

            // Check of permission
            if (!CanManageTuneInfo(transferParam, dataStructure))
            {
                // Not authorized to manage indexes of dsd
                return new ForbidResult();
            }

            var tuneInfo = await _commonManager.GetTuneInfo(dataStructure, transferParam, cancellationToken);

            return new OkObjectResult(tuneInfo);
        }

        /// <summary>
        /// Execute in data spaces of type "HPR" (High Performance Read) the related optimization for the data and referential metadata of all dataflows of the given DSD and, if applicable, make previous changes immediately available in data extractions.
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace where to optimize the DSD. <br></br> Example: <code>design</code></param>
        /// <param name="dsd">The SDMX ID of the DSD to be optimized <br></br> Format: 'AGENCYID:DSD ID(VERSION)' <br></br> Example: <code>OECD:MEI(1.0)</code></param>
        /// <remarks>
        /// Example: Optimize the DSD 'OECD:MEI(1.0)' in the 'design' dataspace
        /// 
        ///     POST /optimize/dsd
        ///     {
        ///        "dataspace": "design",
        ///        "dsd": "OECD:MEI(1.0)"
        ///     }
        ///     
        /// </remarks>
        /// <response code="200">Successfully completed</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="403">Not authorized.</response>
        /// <response code="404">Not found.</response>
        /// <response code="501">Not Implemented.</response>
        [HttpPost]
        [MapToApiVersion("3")]
        [Route("optimize/dsd")]
        public async Task<ActionResult<OperationResult>> OptimizeDsd(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dsd)
        {
            var cancellationToken = CancellationToken.None;
            var lang = _configuration.DefaultLanguageCode ?? Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;

            var destinationDataSpace = dataspace.GetSpaceInternal(_configuration, lang);
            
            // Same message is fine for tune and optimize, not supported by MariaDb.
            if (!destinationDataSpace.CheckTuneDsdIsSupported())
            {
                var message = GetLocalisedResource(Localization.ResourceId.NotSupportedTuneDsdAndMariaDb);
                var operationResult = new OperationResult(false, message);
                return StatusCode(StatusCodes.Status501NotImplemented, operationResult);
            }

            if (!destinationDataSpace.OptimizedForHighPerformanceReads)
            {
                var message = GetLocalisedResource(Localization.ResourceId.NotSupportedOptimizeDsdWithDataspace);
                var operationResult = new OperationResult(false, message);
                return StatusCode(StatusCodes.Status501NotImplemented, operationResult);
            }

            var dataStructure = dsd.GetDsd(lang);
            var transferParam = new TransferParam()
            {
                Principal = new DotStatPrincipal(_contextAccessor?.HttpContext?.User, _authConfiguration.ClaimsMapping),
                TransactionType = TransactionType.OptimizeDsd,
                CultureInfo = CultureInfo.GetCultureInfo(lang),
                DestinationDataspace = destinationDataSpace,
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null
            };

            // Check of permission
            if (!CanManageTuneInfo(transferParam, dataStructure))
            {
                // Not authorized to manage indexes of dsd
                return new ForbidResult();
            }

            var dsdObj = _mappingStoreDataAccess.GetDsd(destinationDataSpace?.Id, dataStructure.AgencyId,
                dataStructure.Id, dataStructure.Version, false);

            if (dsdObj == null)
            {
                //Dsd not found
                return new ActionResult<OperationResult>(new NotFoundObjectResult(string.Format(
                    GetLocalisedResource(Localization.ResourceId.DsdNotFoundInDataspace, lang),
                    dataStructure.AgencyId, dataStructure.Id, dataStructure.Version, destinationDataSpace?.Id)));
            }

            try
            {
                var dotStatDbService = _dotStatDbServiceResolver(transferParam?.DestinationDataspace?.Id);
                var unitOfWork = _unitOfWorkResolver(transferParam?.DestinationDataspace?.Id);

                transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);
                transferParam.TransactionType = TransactionType.TuneDsd;

                var transactionResult = new TransactionResult
                {
                    TransactionType = transferParam.TransactionType,
                    TransactionId = transferParam.Id,
                    DestinationDataSpaceId = transferParam.DestinationDataspace?.Id,
                    Aftefact = dsdObj.FullId,
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.Queued
                };

                await dotStatDbService.FillIdsFromDisseminationDb(dsdObj, cancellationToken);

                LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);

                var message = string.Format(
                    GetLocalisedResource(Localization.ResourceId.SubmissionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id);
                Log.Notice(message);

                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, dsdObj.FullId, transferParam.Principal,
                    transferParam.DestinationDataspace?.Id, string.Empty,
                    transferParam.TransactionType, transferParam.TargetVersion,
                    _configuration.ServiceId, cancellationToken);

                //Function with the main steps to execute the request
                async Task MainTask(CancellationToken backgroundCancellationToken)
                {
                    using var internalCancellationToken = backgroundCancellationToken.SetBackgroundTaskCompletionSteps(dotStatDbService, transferParam, cancellationToken);

                    try
                    {
                        await _commonManager.OptimizeDsd(dsdObj, transferParam, transaction, cancellationToken);
                    }
                    catch (DotStatException exception)
                    {
                        Log.Warn(GetLocalisedResource(Localization.ResourceId.ErrorChangingIndexType, transferParam.CultureInfo.TwoLetterISOLanguageName));
                        Log.Error(exception);
                    }
                    catch (Exception exception)
                    {
                        if (exception is not TaskCanceledException && !internalCancellationToken.Token.IsCancellationRequested)
                        {
                            Log.Error(string.Format(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurred, transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                            Log.Fatal(exception);
                        }
                    }
                    finally
                    {
                        transactionResult.TransactionStatus = await dotStatDbService.GetFinalTransactionStatus(transferParam.Id, CancellationToken.None);

                        await _mailService.SendMail(
                            transactionResult,
                            transferParam.CultureInfo.TwoLetterISOLanguageName,
                            _authorizationManagement.IsAuthorized(transferParam.Principal, PermissionType.AdminRole), SendEmailOptionsEnum.Always
                        );
                    }

                    await Task.CompletedTask;
                }

                //Delegate to check if this request can begin to process, or if it should be placed back to the queue.
                async Task<bool> CanBeProcessed(CancellationToken backgroundCancellationToken)
                {
                    Log.SetTransactionId(transferParam.Id);
                    Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
                    return await dotStatDbService.TryNewTransactionWithDsdId(transaction, dsdObj.DbId,
                        null, dsdObj.AgencyId, dsdObj.Base.Id, dsdObj.Version, backgroundCancellationToken, onlyLockTransaction: true);
                }

                //Function to execute to notify/log that the request is still in progress.
                async Task NotifyInProgressTask(CancellationToken backgroundCancellationToken) => await backgroundCancellationToken.SetStillProcessingLogging(transferParam);

                //Action to execute when the application is shutting down
                async void CallBackAction() => await unitOfWork.TransactionRepository.MarkTransactionAsCanceled(transferParam.Id);

                _backgroundQueue.Enqueue(new TransactionQueueItem(CanBeProcessed, MainTask, NotifyInProgressTask, CallBackAction));

                return new OperationResult(true, message);
            }
            catch (Exception exception)
            {
                if (transferParam.Id == 0)
                {
                    Log.Error(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurredNoId, transferParam.CultureInfo.TwoLetterISOLanguageName));
                }
                else
                {
                    Log.Error(string.Format(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurred, transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                }
                Log.Fatal(exception);
                throw;
            }
        }

        /// <summary>
        /// Get the status of the "HPR" (High Performance Read) data and metadata optimization for the given DSD.
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace. <br></br> Example: <code>design</code></param>
        /// <param name="dsd">The SDMX ID of the DSD <br></br> Format: 'AGENCYID:DSD ID(VERSION)' <br></br> Example: <code>OECD:MEI(1.0)</code></param>
        /// <remarks>
        /// Example: Get the optimization information for the DSD 'OECD:MEI(1.0)', found in the 'design' dataspace
        /// 
        ///     POST /optimize/info
        ///     {
        ///        "dataspace": "design",
        ///        "dsd": "OECD:MEI(1.0)"
        ///     }
        /// </remarks>
        /// <response code="200">Successfully completed </response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="403">Not authorized.</response>
        /// <response code="404">Not found.</response>
        [HttpPost]
        [MapToApiVersion("3")]
        [Route("optimize/info")]
        public async Task<ActionResult<OperationResult>> OptimizeInfo(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dsd
        )
        {
            var lang = _configuration.DefaultLanguageCode ?? Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
            var destinationDataSpace = dataspace.GetSpaceInternal(_configuration, lang);

            // Same message is fine for tune and optimize, not supported by MariaDb.
            if (!destinationDataSpace.CheckTuneDsdIsSupported())
            {
                var message = GetLocalisedResource(Localization.ResourceId.NotSupportedTuneDsdAndMariaDb);
                var operationResult = new OperationResult(false, message);
                return StatusCode(StatusCodes.Status501NotImplemented, operationResult);
            }

            if (!destinationDataSpace.OptimizedForHighPerformanceReads)
            {
                var message = GetLocalisedResource(Localization.ResourceId.NotSupportedOptimizeDsdWithDataspace);
                var operationResult = new OperationResult(false, message);
                return StatusCode(StatusCodes.Status501NotImplemented, operationResult);
            }

            var dataStructure = dsd.GetDsd(lang);
            var cancellationToken = CancellationToken.None;
            var transferParam = new TransferParam
            {
                DestinationDataspace = destinationDataSpace,
                Principal = new DotStatPrincipal(_contextAccessor?.HttpContext?.User, _authConfiguration.ClaimsMapping)
            };

            // Check of permission
            if (!CanManageTuneInfo(transferParam, dataStructure))
            {
                // Not authorized to manage indexes of dsd
                return new ForbidResult();
            }

            var dsdOptimizeInfo = await _commonManager.GetOptimizeInfo(dataStructure, transferParam, cancellationToken);

            return new OkObjectResult($"Data: {dsdOptimizeInfo.DsdDataOptimized}, Metadata: {dsdOptimizeInfo.DsdMetadataOptimized}");
        }
        private async Task<ActionResult<OperationResult>> CreateAllMappingsets<T>(T transferParam, SendEmailOptionsEnum sendEmailOption, CancellationToken cancellationToken) where T : TransferParam
        {
            try
            {
                var dotStatDbService = _dotStatDbServiceResolver(transferParam.DestinationDataspace.Id);
                var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace.Id);

                transferParam.Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);
                
                if (!_authorizationManagement.IsAuthorized(transferParam.Principal, PermissionType.AdminRole))
                    throw new TransferUnauthorizedException();

                transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);

                var transactionResult = new TransactionResult()
                {
                    TransactionType = transferParam.TransactionType,
                    TransactionId = transferParam.Id,
                    DataSource = transferParam.DataSource,
                    SourceDataSpaceId = transferParam.SourceDataspace?.Id,
                    DestinationDataSpaceId = transferParam.DestinationDataspace?.Id,
                    Aftefact = SpecialTransactionArtefactIds.All,
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.Queued
                };

                LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);

                var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                    transferParam.Id, SpecialTransactionArtefactIds.All, transferParam.Principal,
                    sourceDataSpace: null, dataSource: null, TransactionType.InitAllMappingSets, 
                    requestedTargetVersion: null, _configuration.ServiceId, cancellationToken, true);

                var message = string.Format(
                    LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.SubmissionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName),
                    transferParam.Id);

                Log.Notice(message);

                //Function with the main steps to execute the request
                async Task mainTask(CancellationToken backgroundCancellationToken)
                {
                    using var internalCancellationToken = backgroundCancellationToken.SetBackgroundTaskCompletionSteps(dotStatDbService, transferParam, cancellationToken);
                    try
                    {
                        //Manage all mappingSets of the dataFlows belonging to the Dsd
                        if (!await dotStatDbService.InitializeAllMappingSets(transaction, _mappingStoreDataAccess, internalCancellationToken.Token))
                        {
                            Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.CreateAllMappingsetsFailed,
                                transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                        }
                        else
                        {
                            Log.Notice(string.Format(LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.CreateAllMappingsetsCompleted,
                                transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                        }
                    }
                    catch (Exception exception)
                    {
                        if (exception is not TaskCanceledException && !internalCancellationToken.Token.IsCancellationRequested)
                        {
                            Log.Error(string.Format(LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.CreateAllMappingsetsFailed,
                                transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));

                            Log.Fatal(exception);
                            await dotStatDbService.CleanUpFailedTransaction(transaction);
                        }
                    }
                    finally
                    {
                        transactionResult.TransactionStatus = await dotStatDbService.GetFinalTransactionStatus(transferParam.Id, CancellationToken.None);
                        await _mailService.SendMail(
                            transactionResult,
                            transferParam.CultureInfo.TwoLetterISOLanguageName,
                            _authorizationManagement.IsAuthorized(transferParam.Principal, PermissionType.AdminRole),
                            sendEmailOption
                        );
                    }

                    await Task.CompletedTask;
                }


                //Delegate to check if this request can begin to process, or if it should be placed back to the queue.
                async Task<bool> canBeProcessed(CancellationToken backgroundCancellationToken)
                {
                    Log.SetTransactionId(transferParam.Id);
                    Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
                    return await dotStatDbService.TryNewTransactionWithNoDsd(transaction, backgroundCancellationToken, onlyLockTransaction: true);
                }

                //Function to execute to notify/log that the request is still in progress.
                async Task notifyInProgressTask(CancellationToken backgroundCancellationToken) => await backgroundCancellationToken.SetStillProcessingLogging(transferParam);

                //Action to execute when the application is shutting down
                async void callBackAction() => await unitOfWork.TransactionRepository.MarkTransactionAsCanceled(transferParam.Id);

                _backgroundQueue.Enqueue(new TransactionQueueItem(canBeProcessed, mainTask, notifyInProgressTask, callBackAction));

                return new OperationResult(true, message);
            }
            catch (Exception exception)
            {
                if (transferParam.Id == 0)
                {
                    Log.Error(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurredNoId, transferParam.CultureInfo.TwoLetterISOLanguageName));
                }
                else
                {
                    Log.Error(string.Format(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurred, transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                }
                Log.Fatal(exception);
                throw;
            }
        }

        private bool CanManageTuneInfo(TransferParam transferParam, DataStructureMutableCore dataStructure)
        {
            //CanDeleteStructuralMetadata && CanUpdateStructuralMetadata
            //&& CanImportStructures && CanReadStructuralMetadata

            return _authorizationManagement.IsAuthorized(
                       transferParam.Principal,
                       transferParam.DestinationDataspace?.Id,
                       dataStructure.AgencyId,
                       dataStructure.Id,
                       dataStructure.Version,
                       PermissionType.CanDeleteStructuralMetadata)
                   && _authorizationManagement.IsAuthorized(
                       transferParam.Principal,
                       transferParam.DestinationDataspace?.Id,
                       dataStructure.AgencyId,
                       dataStructure.Id,
                       dataStructure.Version,
                       PermissionType.CanUpdateStructuralMetadata)
                   && _authorizationManagement.IsAuthorized(
                       transferParam.Principal,
                       transferParam.DestinationDataspace?.Id,
                       dataStructure.AgencyId,
                       dataStructure.Id,
                       dataStructure.Version,
                       PermissionType.CanImportStructures)
                   && _authorizationManagement.IsAuthorized(
                       transferParam.Principal,
                       transferParam.DestinationDataspace?.Id,
                       dataStructure.AgencyId,
                       dataStructure.Id,
                       dataStructure.Version,
                       PermissionType.CanReadStructuralMetadata);
        }
    }
}
