﻿using System.Reflection;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStat.Transfer.Param;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model;
using System.Threading;
using System.Threading.Tasks;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.Common.Exceptions;
using Transaction = DotStat.Domain.Transaction;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Model;
using DotStat.Transfer.Utils;
using log4net;

namespace DotStat.Transfer.Test.Unit.Manager
{
    [TestFixture]
    public class CommonManagerTest : SdmxUnitTestBase
    {
        private readonly Mock<IMappingStoreDataAccess> _mappingStore = new();
        private readonly Mock<IMetadataStoreRepository> _metadataStoreRepository = new();
        private readonly Mock<IAuthorizationManagement> _authorisation = new();

        private readonly Mock<IUnitOfWork> _unitOfWorkMock = new Mock<IUnitOfWork>();
        private readonly Mock<IDotStatDbService> _dotStatDbServiceMock = new Mock<IDotStatDbService>();

        private readonly CommonManager _commonManager;
        private readonly string _dataSpaceId = "DummySpace";

        public CommonManagerTest()
        {

            UnitOfWorkResolver unitOfWorkResolver = dataSpaceId => _unitOfWorkMock.Object;
            DotStatDbServiceResolver dotStatDbServiceResolver = dataSpaceId => _dotStatDbServiceMock.Object;

            _commonManager = new CommonManager(
                _authorisation.Object,
                _mappingStore.Object,
                unitOfWorkResolver,
                dotStatDbServiceResolver,
                Configuration
                );
        }

        [Test]
        [TestCase(DataCompressionEnum.NONE, DataCompressionEnum.COLUMNSTORE, IndexTypeParamEnum.CLUSTERED_COLUMNSTORE)]
        [TestCase(DataCompressionEnum.COLUMNSTORE, DataCompressionEnum.COLUMNSTORE, IndexTypeParamEnum.CLUSTERED_COLUMNSTORE)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE, DataCompressionEnum.COLUMNSTORE, IndexTypeParamEnum.CLUSTERED_COLUMNSTORE)]
        [TestCase(DataCompressionEnum.NONE, DataCompressionEnum.COLUMNSTORE_ARCHIVE, IndexTypeParamEnum.CLUSTERED_COLUMNSTORE_ARCHIVE)]
        [TestCase(DataCompressionEnum.COLUMNSTORE, DataCompressionEnum.COLUMNSTORE_ARCHIVE, IndexTypeParamEnum.CLUSTERED_COLUMNSTORE_ARCHIVE)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE, DataCompressionEnum.COLUMNSTORE_ARCHIVE, IndexTypeParamEnum.CLUSTERED_COLUMNSTORE_ARCHIVE)]
        public async Task TuneDsd(DataCompressionEnum currentDataCompression, DataCompressionEnum expectedDataCompression, IndexTypeParamEnum indexType)
        {
            var dsd = new Dsd(GetDsd().Base)
            {
                DataCompression = currentDataCompression
            };

            var expectedSuccess = expectedDataCompression != currentDataCompression;

            _mappingStore.Setup(x =>
                    x.GetDsd(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<bool>(), It.IsAny<ResolveCrossReferences>(), It.IsAny<bool>()))
                .Returns(dsd);
         
            _dotStatDbServiceMock.Setup(x => x.ChangeDataCompression(
                    It.IsAny<Dsd>(),
                    It.IsAny<DataCompressionEnum>(),
                    It.IsAny<IMappingStoreDataAccess>(),
                    It.IsAny<CancellationToken>()
                    ))
                .Callback(() => dsd.DataCompression = expectedDataCompression)
                .Returns(Task.FromResult(expectedSuccess));

            _dotStatDbServiceMock.Setup(x =>
                    x.TryNewTransactionWithDsdId(
                        It.IsAny<Transaction>(),
                        It.IsAny<int>(),
                        It.IsAny<int?>(),
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<SdmxVersion>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<bool>()))
                .Returns(Task.FromResult(true));

            _unitOfWorkMock.Setup(x =>
                x.TransactionRepository.MarkTransactionAsCompleted(
                    It.IsAny<int>(),
                    It.IsAny<bool>()))
                .Returns(Task.FromResult(true));

            var dataSpace = new DataspaceInternal { Id = _dataSpaceId };
            var param = new TransferParam
            {
                DestinationDataspace = dataSpace,
                DataCompression = indexType.GetCompression()
            };

            var transaction = new Transaction
            {
                Id = 1,
                ArtefactFullId = dsd.FullId,
                ArtefactDbId = dsd.DbId
            };

            var success = await _commonManager.TuneDsd(dsd, param, transaction, indexType, CancellationToken);
            
            Assert.AreEqual(expectedSuccess, success);
            Assert.AreEqual(expectedDataCompression, dsd.DataCompression);
        }


        [Test]
        public async Task TuneDsdConcurrentTransaction()
        {
            var dsd = GetDsd();

            _dotStatDbServiceMock.Setup(x =>
                    x.TryNewTransactionWithDsdId(
                        It.IsAny<Transaction>(),
                        It.IsAny<int>(),
                        It.IsAny<int?>(),
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<SdmxVersion>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<bool>()))
                .Returns(Task.FromResult(false));

            _unitOfWorkMock.Setup(x =>
                x.TransactionRepository.MarkTransactionAsCompleted(
                    It.IsAny<int>(),
                    It.IsAny<bool>()))
                .Returns(Task.FromResult(true));

            var param = new TransferParam
            {
                DestinationDataspace = new DataspaceInternal { Id = _dataSpaceId }
            };
            var transaction = new Transaction
            {
                Id = 1,
                ArtefactFullId = dsd.FullId,
                ArtefactDbId = dsd.DbId
            };

            var success = await _commonManager.TuneDsd(dsd, param, transaction, IndexTypeParamEnum.CLUSTERED_COLUMNSTORE, CancellationToken);

            Assert.AreEqual(null, success);
        }


        [Test]
        [TestCase(DataCompressionEnum.NONE, IndexTypeEnum.NONCLUSTERED_COLUMNSTORE, false)]
        [TestCase(DataCompressionEnum.NONE, IndexTypeEnum.NONCLUSTERED_COLUMNSTORE, true)]
        [TestCase(DataCompressionEnum.COLUMNSTORE, IndexTypeEnum.CLUSTERED_COLUMNSTORE, false)]
        [TestCase(DataCompressionEnum.COLUMNSTORE, IndexTypeEnum.CLUSTERED_COLUMNSTORE, true)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE, IndexTypeEnum.CLUSTERED_COLUMNSTORE_ARCHIVE, true)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE, IndexTypeEnum.CLUSTERED_COLUMNSTORE_ARCHIVE, false)]
        public async Task GetTuneInfo(DataCompressionEnum dataCompression, IndexTypeEnum expectedIndexType, bool dsdExists)
        {
            var dsd = GetDsd();
            _mappingStore.Setup(x =>
                    x.GetDsd(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<bool>(), It.IsAny<ResolveCrossReferences>(), It.IsAny<bool>()))
                .Returns(dsdExists? dsd : (Dsd)null);

            _unitOfWorkMock.Setup(x => x.ArtefactRepository.FillMeta(
                    It.IsAny<Dsd>(), It.IsAny<CancellationToken>(), It.IsAny<bool>()))
                .Callback(()=> 
                    dsd.DataCompression = dataCompression)
                .Returns(Task.CompletedTask);

            var dataStructure = dsd.FullId.GetDsd("xx");
            var param = new TransferParam
            {
                DestinationDataspace = new DataspaceInternal{Id = _dataSpaceId }
            };

            if (dsdExists)
            {
                var tuneInfo = await _commonManager.GetTuneInfo(dataStructure, param, CancellationToken);
                Assert.AreEqual(expectedIndexType.ToString(), tuneInfo.IndexType);
            }
            else
            {
                var exe = Assert.ThrowsAsync<DotStatException>(() => _commonManager.GetTuneInfo(dataStructure, param, CancellationToken));

                var expectedErrorMessage = string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdNotFoundInDataspace),
                    dataStructure.AgencyId,
                    dataStructure.Id,
                    dataStructure.Version,
                    param.DestinationDataspace?.Id);

                Assert.AreEqual(expectedErrorMessage, exe.Message);
            }
        }

        [TearDown]
        public void TearDown()
        {
            _authorisation.Invocations.Clear();
            _metadataStoreRepository.Invocations.Clear();
            _mappingStore.Invocations.Clear();
            _unitOfWorkMock.Invocations.Clear();
            _dotStatDbServiceMock.Invocations.Clear();
        }
    }
}
