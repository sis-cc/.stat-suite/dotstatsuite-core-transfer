﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.DB;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Controllers;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model;
using DotStat.Transfer.Utils;
using System.Globalization;
using DotStat.Db.Exception;
using DotStat.Common.Localization;
using DotStat.Common.Exceptions;
using System.Linq;
using Castle.Core.Logging;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class PitControllerTests : SdmxUnitTestBase
    {
        private readonly PointInTimeController _controller;
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IMappingStoreDataAccess> _mappingStore = new Mock<IMappingStoreDataAccess>();
        private readonly Mock<IAuthorizationManagement> _authManagement = new Mock<IAuthorizationManagement>();
        private readonly Mock<IMailService> _mailService = new Mock<IMailService>();
        private readonly Mock<BackgroundQueue> _backgroundQueueMock = new Mock<BackgroundQueue>();
        
        private readonly Mock<IDotStatDb> _dotStatDbMock = new Mock<IDotStatDb>();
        private readonly Mock<IUnitOfWork> _unitOfWorkMock = new Mock<IUnitOfWork>();
        private readonly Mock<IDotStatDbService> _dotStatDbServiceMock = new Mock<IDotStatDbService>();

        private readonly string _dataSpace = "design";
        private readonly Dataflow _dataflow;
        private readonly AuthConfiguration _authConfig;

        public PitControllerTests()
        {
            this.Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = _dataSpace}
            };
            this.Configuration.DefaultLanguageCode = "en";

            _dataflow = GetDataflow();

            _dotStatDbServiceMock.Setup(mock => 
                mock.TryNewTransaction(
                    It.IsAny<Transaction>(), It.IsAny<Dataflow>(),
                    It.IsAny<IMappingStoreDataAccess>(), It.IsAny<CancellationToken>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(true));

            _dotStatDbServiceMock.Setup(mock => 
                mock.Rollback(It.IsAny<Dataflow>(), It.IsAny<IMappingStoreDataAccess>(), It.IsAny<CancellationToken>()));

            _authManagement
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    _dataSpace,
                    _dataflow.AgencyId,
                    _dataflow.Code,
                    _dataflow.Version.ToString(),
                    PermissionType.CanImportData
                ))
                .Returns(true);
            
            _mappingStore.Setup(mock =>
                    mock.GetDataflow(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<ResolveCrossReferences>(), It.IsAny<bool>()))
                .Returns(new Dataflow(_dataflow.Base, _dataflow.Dsd));

            _unitOfWorkMock.Setup(mock => 
                mock.TransactionRepository.MarkTransactionAsCompleted(
                    It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(true));
            
            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _backgroundQueueMock
                .Setup(x => x.Enqueue(It.IsAny<TransactionQueueItem>()))
                .Callback((TransactionQueueItem transactionQueueItem) =>
                {
                    transactionQueueItem.MainTask.Invoke(new CancellationToken());
                });

            _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            Func<string, IDotStatDb> dotStatDbResolver = dataSpaceId => _dotStatDbMock.Object;
            UnitOfWorkResolver unitOfWorkResolver = dataSpaceId => _unitOfWorkMock.Object;
            DotStatDbServiceResolver dotStatDbServiceResolver = dataSpaceId => _dotStatDbServiceMock.Object;

            var commonManager = new CommonManager(
                _authManagement.Object, _mappingStore.Object, unitOfWorkResolver, 
                dotStatDbServiceResolver, Configuration);

            _controller = new PointInTimeController(
                _contextAccessorMock.Object,
                commonManager, 
                Configuration,
                _authConfig,
                _mailService.Object,
                _backgroundQueueMock.Object,
                _authManagement.Object,
                unitOfWorkResolver,
                dotStatDbServiceResolver,
                _mappingStore.Object
             );
        }


        [Test]
        //No PITReleaseDate in param
        [TestCase("design", "IT1:264D_264_SALDI(1.0)", "", DbTableVersion.A, "", false)]
        //PITReleaseDate in param future
        [TestCase("design", "IT1:264D_264_SALDI(1.0)", "2059-09-09T00:00:00+02:00", DbTableVersion.A, "", false)]
        //PITReleaseDate in param past
        [TestCase("design", "IT1:264D_264_SALDI(1.0)", "2022-02-02T00:00:00+02:00", DbTableVersion.A, "", false)]
        //Non existing PIT
        [TestCase("design", "IT1:264D_264_SALDI(1.0)", "", null, "", false)]
        //PIT already released
        [TestCase("design", "IT1:264D_264_SALDI(1.0)", "", DbTableVersion.A, "2024-01-01T00:00:00+02:00", false)]
        //Another transaction in progress
        [TestCase("design", "IT1:264D_264_SALDI(1.0)", "2059-09-09T00:00:00+02:00", DbTableVersion.A, "", true)]
        public async Task PitInfoTest(string dataspace, string dataflow, string newPitReleaseDateStr, DbTableVersion? existingPITVersion, string existingPitReleaseDateStr, bool existsActiveTransaction)
        {
            var pitRestorationDate = DateTime.ParseExact("2024-05-05T00:00:00+02:00", "yyyy-MM-ddTHH:mm:ss.FFFzzz", CultureInfo.InvariantCulture);
            var currentDateTime = DateTime.ParseExact("2024-02-02T00:00:00+02:00", "yyyy-MM-ddTHH:mm:ss.FFFzzz", CultureInfo.InvariantCulture);
            var isValidNewPitReleaseDate = DateTime.TryParseExact(newPitReleaseDateStr, "yyyy-MM-ddTHH:mm:ss.FFFzzz", CultureInfo.InvariantCulture, DateTimeStyles.None, out var newPitReleaseDate);
            var existingPitHasReleaseDate = DateTime.TryParseExact(existingPitReleaseDateStr, "yyyy-MM-ddTHH:mm:ss.FFFzzz", CultureInfo.InvariantCulture, DateTimeStyles.None, out var existingPitReleaseDate);

            _unitOfWorkMock.Setup(mock =>
            mock.ArtefactRepository.FillMeta(
                It.IsAny<Dsd>(),
                It.IsAny<CancellationToken>(),
                It.IsAny<bool>()))
            .Returns(Task.CompletedTask)
            .Callback(() =>
            {
                if (existingPITVersion is not null)
                {
                    _dataflow.Dsd.PITRestorationDate = pitRestorationDate;
                    _dataflow.Dsd.PITVersion = (char)existingPITVersion;
                    _dataflow.Dsd.PITReleaseDate = existingPitHasReleaseDate? existingPitReleaseDate : null;
                    _dataflow.Dsd.LiveVersion = (char)DbTableVersions.GetNewTableVersion(
                        (DbTableVersion)DbTableVersions.GetDbTableVersion((char)existingPITVersion));
                }
                else
                {
                    _dataflow.Dsd.LiveVersion = DbTableVersions.GetDefaultLiveVersion();
                }

            });

            _dotStatDbServiceMock.Setup(mock =>
            mock.ApplyPITRelease(
                It.IsAny<Dsd>(),
                It.IsAny<bool>(),
                It.IsAny<IMappingStoreDataAccess>()))
            .Returns(Task.CompletedTask)
            .Callback(() =>
            {
                _dataflow.Dsd.PITVersion = null;
                _dataflow.Dsd.PITReleaseDate = null;
                _dataflow.Dsd.PITRestorationDate = null;
            });

            if (existsActiveTransaction)
            {
                _dotStatDbServiceMock.Setup(mock =>
                mock.TryNewTransaction(
                    It.IsAny<Transaction>(),
                    It.IsAny<Dataflow>(),
                    It.IsAny<IMappingStoreDataAccess>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<bool>()))
                .Returns(Task.FromResult(false));

                _unitOfWorkMock.Setup(mock =>
                mock.TransactionRepository.CreateTransactionItem(
                    It.IsAny<int>(),
                    It.IsAny<string>(),
                    It.IsAny<DotStatPrincipal>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<TransactionType>(),
                    It.IsAny<TargetVersion?>(),
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<bool>()))
                .Returns(Task.FromResult(new Transaction()));

                _unitOfWorkMock.Setup(mock =>
                mock.TransactionRepository.GetInProgressTransactions(
                    It.IsAny<int>(),
                    It.IsAny<int>(),
                    It.IsAny<CancellationToken>()))
                .Returns(new List<Transaction>().ToAsyncEnumerable());

                Assert.ThrowsAsync<DotStatException>(() =>
                    _controller.PitInfo(dataspace, dataflow, newPitReleaseDateStr)
                );
                return;
            }

            //Non existing PIT
            if (existingPITVersion == null)
            {
                Assert.ThrowsAsync<TableVersionException>(() =>
                    _controller.PitInfo(dataspace, dataflow, newPitReleaseDateStr)
                );
                return;
            }

            var isPitAlreadyReleased = existingPitHasReleaseDate && currentDateTime > existingPitReleaseDate; 
            //PIT already released
            if (isPitAlreadyReleased)
            {
                Assert.ThrowsAsync<TableVersionException>(() =>
                    _controller.PitInfo(dataspace, dataflow, newPitReleaseDateStr)
                );
                return;
            }

            var result = await _controller.PitInfo(dataspace, dataflow, newPitReleaseDateStr);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);

            //PITRelease in param
            if (isValidNewPitReleaseDate)
            {
                Assert.IsInstanceOf<ObjectResult>(result.Result);
                var createdObjectResult = (ObjectResult)result.Result;
                Assert.AreEqual(StatusCodes.Status201Created, createdObjectResult.StatusCode);

                //PITRelease param is in the past
                if (newPitReleaseDate < currentDateTime)
                {
                    var expectedResponse = new JObject
                    {
                        new JProperty("Success", true),
                        new JProperty("Message", string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.PITReleaseDatePastUpdated),  _dataflow.Dsd.FullId)),
                        new JProperty("Detail", null),
                    };

                    var createdObjectResultValue = JObject.FromObject(createdObjectResult.Value);
                    Assert.AreEqual(expectedResponse.ToString(), createdObjectResultValue.ToString());
                }
                else //PITRelease param is in the future
                {
                    var expectedDetail = new JObject
                    {
                        new JProperty("PITVersion", existingPITVersion.ToString()),
                        new JProperty("PITReleaseDate", newPitReleaseDate),
                        new JProperty("PITRestorationDate", pitRestorationDate),
                    };

                    var expectedResponse = new JObject
                    {
                        new JProperty("Success", true),
                        new JProperty("Message", LocalizationRepository.GetLocalisedResource(Localization.ResourceId.PITReleaseDateUpdated)),
                        new JProperty("Detail", expectedDetail),
                    };

                    var createdObjectResultValue = JObject.FromObject(createdObjectResult.Value);
                    Assert.AreEqual(expectedResponse.ToString(), createdObjectResultValue.ToString());
                }
            }
            else //No PITReleaseDate in param
            {
                var serverTimeZone = TimeZoneInfo.Local;

                var expectedValue = new JObject
                {
                    new JProperty("PITVersion", existingPITVersion.ToString()),
                    new JProperty("PITReleaseDate", existingPitHasReleaseDate? existingPitReleaseDate : null),
                    new JProperty("PITRestorationDate", pitRestorationDate)
                };

                var okObjectResult = (OkObjectResult)result.Result;
                var okObjectResultValue = okObjectResult.Value;
                Assert.AreEqual(expectedValue.ToString(), okObjectResultValue.ToString());
            }
        }


        [Test]
        public async Task RollbackTest()
        {
            var result = await _controller.Rollback("design", _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);
        }

        [Test]
        public async Task RestoreTest()
        {
            var result = await _controller.Restore("design", _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);
        }
    }
}
