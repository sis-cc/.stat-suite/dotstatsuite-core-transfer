﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStatServices.Transfer;
using DotStatServices.Transfer.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading;
using DotStat.Common.Localization;
using DotStat.Transfer.Manager;
using Estat.Sdmxsource.Extension.Constant;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using DotStatServices.Transfer.BackgroundJob;
using Org.Sdmxsource.Sdmx.Api.Model;
using System.Threading.Tasks;
using DotStat.Transfer.Interface;
using DotStat.Db.DB;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using System.Linq;
using DotStat.Transfer.Model;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class DataManagementControllerTests : SdmxUnitTestBase
    {
        private readonly Dataflow _dataflow;
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IAuthorizationManagement> _authManagementMock = new Mock<IAuthorizationManagement>();
        private const string ResetDataSpace = "reset";
        private const string StableDataSpace = "stable";

        private readonly Mock<IMappingStoreDataAccess>
            _mappingStoreDataAccessMock = new Mock<IMappingStoreDataAccess>();

        private readonly Mock<IDotStatDb> _dotStatDbMock = new Mock<IDotStatDb>();
        private readonly Mock<IUnitOfWork> _unitOfWorkMock = new Mock<IUnitOfWork>();
        private readonly Mock<IDotStatDbService> _dotStatDbServiceMock = new Mock<IDotStatDbService>();
        
        private readonly Mock<IMailService> _mailServiceMock = new Mock<IMailService>();
        private readonly Mock<BackgroundQueue> _backgroundQueueMock = new Mock<BackgroundQueue>();

        private readonly AuthConfiguration _authConfig;
        private readonly CommonManager _commonManager;
        private readonly DataManagementController _controller;
        private int MaxDataImportTimeInMinutes;

        public DataManagementControllerTests()
        {
            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            _dataflow = GetDataflow();

            SetupAuthorizationManagement(ResetDataSpace, PermissionType.CanImportData);

            SetupMappingStoreDataAccess(_dataflow);
            
            Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = ResetDataSpace},
                new DataspaceInternal() {Id = StableDataSpace}
            };

            Configuration.DefaultLanguageCode = "en";
            
                        
            Func<string, IDotStatDb> dotStatDbResolver = dataSpaceId => _dotStatDbMock.Object;
            UnitOfWorkResolver unitOfWorkResolver = dataSpaceId => _unitOfWorkMock.Object;
            DotStatDbServiceResolver dotStatDbServiceResolver = dataSpaceId => _dotStatDbServiceMock.Object;

            MaxDataImportTimeInMinutes = _unitOfWorkMock.Object.MaxDataImportTimeInMinutes;

            _commonManager = new CommonManager(
                _authManagementMock.Object, _mappingStoreDataAccessMock.Object,
                unitOfWorkResolver, dotStatDbServiceResolver, Configuration);
            
            _controller = new DataManagementController(_contextAccessorMock.Object, _authConfig,
                _authManagementMock.Object, _mappingStoreDataAccessMock.Object, Configuration,
                _commonManager, _mailServiceMock.Object, _backgroundQueueMock.Object,
                unitOfWorkResolver, dotStatDbServiceResolver);

        }

        [Test]
        public void InitDatabaseObjectsOfDataflowInvalidDataflow()
        {
            var exception = Assert.ThrowsAsync<ArgumentException>(() =>
                _controller.InitDatabaseObjectsOfDataflow(ResetDataSpace, "NOAGENCY:NOID(1.0)"));

            StringAssert.AreEqualIgnoringCase(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowNotFoundInDataspace),
                exception.Message);
        }

        [Test]
        public async Task InitDatabaseObjectsOfDataflowForbiddenAccess()
        {
            var result = await _controller.InitDatabaseObjectsOfDataflow(StableDataSpace, _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<ForbidResult>(result.Result);
        }

        [Test]
        public async Task InitDatabaseObjectsOfDataflowSuccessCreated()
        {
            SetupArtefactRepository(DataflowInDataDatabase.NotExists);
            SetupTransactionRepository(ConcurrentTransactionState.NotExists);
            SetupCodeListRepository();

            var result = await _controller.InitDatabaseObjectsOfDataflow(ResetDataSpace, _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<CreatedResult>(result.Result);

            var createdObjectResult = (CreatedResult)result.Result;
            Assert.AreEqual((int)HttpStatusCode.Created, createdObjectResult.StatusCode);

            Assert.IsInstanceOf<OperationResult>(createdObjectResult.Value);
            Assert.IsTrue(((OperationResult)createdObjectResult.Value).Success);
        }

        [Test]
        public async Task InitDatabaseObjectsOfDataflowConcurrentTransaction()
        {
            SetupArtefactRepository(DataflowInDataDatabase.NotExists);
            SetupTransactionRepository(ConcurrentTransactionState.Exists);

            var result = await _controller.InitDatabaseObjectsOfDataflow(ResetDataSpace, _dataflow.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<ConflictObjectResult>(result.Result);

            var conflictObjectResult = (ConflictObjectResult)result.Result;
            Assert.AreEqual((int)HttpStatusCode.Conflict, conflictObjectResult.StatusCode);

            Assert.IsInstanceOf<OperationResult>(conflictObjectResult.Value);
            Assert.IsFalse(((OperationResult)conflictObjectResult.Value).Success);
        }
        
        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public async Task TuneDsdPermissions(bool hasPermissions)
        {
            SetupArtefactRepository(DataflowInDataDatabase.Exists);
            SetupTransactionRepository(ConcurrentTransactionState.Exists);
            SetupMappingStore(_dataflow.Dsd);
            SetupAuthorizationManagementTuneManagement(ResetDataSpace, hasPermissions);

            var result = await _controller.TuneDsd(ResetDataSpace, _dataflow.Dsd.FullId, IndexTypeParamEnum.CLUSTERED_COLUMNSTORE);
            Assert.IsNotNull(result);
            if (hasPermissions)
            {
                Assert.IsNotNull(result.Value);
                Assert.IsInstanceOf<OperationResult>(result.Value);
                Assert.AreEqual("The request with ID 1 was successfully registered.", result.Value.Message);
            }
            else
            {
                Assert.IsNotNull(result.Result);
                Assert.IsInstanceOf<ForbidResult>(result.Result);
            }
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public async Task TuneDsdExists(bool exists)
        {
            SetupArtefactRepository(DataflowInDataDatabase.Exists);
            SetupTransactionRepository(ConcurrentTransactionState.Exists);
            SetupAuthorizationManagementTuneManagement(ResetDataSpace, true);
            SetupMappingStore(exists ? _dataflow.Dsd : null);

            var result = await _controller.TuneDsd(ResetDataSpace, _dataflow.Dsd.FullId, IndexTypeParamEnum.CLUSTERED_COLUMNSTORE);
            
            Assert.IsNotNull(result);
            if (exists)
            {
                Assert.IsNotNull(result.Value);
                Assert.IsInstanceOf<OperationResult>(result.Value);
                Assert.AreEqual("The request with ID 1 was successfully registered.", result.Value.Message);
            }
            else
            {
                Assert.IsNotNull(result.Result);
                Assert.IsInstanceOf<NotFoundObjectResult>(result.Result);
            }
        }
       
        [Test]
        [TestCase(DataCompressionEnum.COLUMNSTORE)]
        [TestCase(DataCompressionEnum.COLUMNSTORE_ARCHIVE)]
        [TestCase(DataCompressionEnum.NONE)]
        public async Task TuneInfo(DataCompressionEnum expectedDataCompression)
        {
            SetupArtefactRepository(DataflowInDataDatabase.Exists);
            SetupTransactionRepository(ConcurrentTransactionState.Exists);
            SetupAuthorizationManagementTuneManagement(ResetDataSpace, true);

            _dataflow.Dsd.DataCompression = expectedDataCompression;
            SetupMappingStore(_dataflow.Dsd);

            _unitOfWorkMock.Setup(m =>
                    m.ArtefactRepository.FillMeta(
                        It.IsAny<Dsd>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<bool>()
                        ))
                    .Returns(Task.CompletedTask);


            var result = await _controller.TuneInfo(ResetDataSpace, _dataflow.Dsd.FullId);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);

            var okObjectResult = (OkObjectResult)result.Result;
            Assert.IsNotNull(okObjectResult);
            Assert.IsNotNull(okObjectResult.Value);
            Assert.IsInstanceOf<TuneInfo>(okObjectResult.Value);

            var tuneInfo = okObjectResult.Value as TuneInfo;
            Assert.IsNotNull(tuneInfo);
            var expectedTuneInfo = new TuneInfo(expectedDataCompression);
            Assert.AreEqual(expectedTuneInfo.IndexType, tuneInfo.IndexType);
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public async Task TuneInfoDsdExists(bool exists)
        {
            SetupArtefactRepository(DataflowInDataDatabase.Exists);
            SetupTransactionRepository(ConcurrentTransactionState.Exists);
            SetupAuthorizationManagementTuneManagement(ResetDataSpace, true);
            SetupMappingStore(exists ? _dataflow.Dsd : null);

            if (!exists)
            {
                var error = Assert.ThrowsAsync<DotStatException>(() => _controller.TuneInfo(ResetDataSpace, _dataflow.Dsd.FullId));
                var expectedError = new DotStatException(
                    string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdNotFoundInDataspace),
                    _dataflow.Dsd.AgencyId,
                    _dataflow.Dsd.Base.Id,
                    _dataflow.Dsd.Version,
                    ResetDataSpace));

                Assert.AreEqual(expectedError.Message, error.Message);
                return;
            }

            var result = await _controller.TuneInfo(ResetDataSpace, _dataflow.Dsd.FullId);
            
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            Assert.IsInstanceOf<OkObjectResult>(result.Result);
        }
        
        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public async Task TuneInfoPermissions(bool hasPermissions)
        {
            SetupArtefactRepository(DataflowInDataDatabase.Exists);
            SetupTransactionRepository(ConcurrentTransactionState.Exists);
            SetupMappingStore(_dataflow.Dsd);
            SetupAuthorizationManagementTuneManagement(ResetDataSpace, hasPermissions);

            var result = await _controller.TuneInfo(ResetDataSpace, _dataflow.Dsd.FullId);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Result);
            if(hasPermissions)
                Assert.IsInstanceOf<OkObjectResult>(result.Result);
            else
                Assert.IsInstanceOf<ForbidResult>(result.Result);
        }


        [TearDown]
        public void TearDown()
        {
            _authManagementMock.Invocations.Clear();
            _unitOfWorkMock.Invocations.Clear();
            _dotStatDbServiceMock.Invocations.Clear();
        }

        private void SetupMappingStoreDataAccess(Dataflow dataflow = null)
        {
            var df = dataflow ?? _dataflow;

            _mappingStoreDataAccessMock
                .Setup(m => m.GetDataflow(
                    ResetDataSpace,
                    df.AgencyId,
                    df.Code,
                    df.Version.ToString(),
                    true,
                    ResolveCrossReferences.ResolveExcludeAgencies,
                    It.IsAny<bool>()
                ))
                .Returns(df);

            _mappingStoreDataAccessMock
                .Setup(m => m.GetDataflow(
                    ResetDataSpace,
                    It.IsAny<string>(),
                    It.IsNotIn(df.Code),
                    It.IsAny<string>(),
                    true,
                    ResolveCrossReferences.ResolveExcludeAgencies,
                    It.IsAny<bool>()
                ))
                .Throws(new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowNotFoundInDataspace)));

            _mappingStoreDataAccessMock
                .Setup(m => m.GetDsd(
                    ResetDataSpace,
                    df.Dsd.AgencyId,
                    df.Dsd.Code,
                    df.Dsd.Version.ToString(),
                    It.IsAny<bool>(),
                    It.IsAny<ResolveCrossReferences>(),
                    It.IsAny<bool>()
                ))
                .Returns(df.Dsd);

            _mappingStoreDataAccessMock
                .Setup(m => m.GetDsd(
                    ResetDataSpace,
                    It.IsAny<string>(),
                    It.IsNotIn(df.Dsd.Code),
                    It.IsAny<string>(),
                    It.IsAny<bool>(),
                    It.IsAny<ResolveCrossReferences>(),
                    It.IsAny<bool>()
                ))
                .Throws(new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdNotFoundInDataspace)));

        }

        private void SetupArtefactRepository(DataflowInDataDatabase dataflowExistsInDataDb)
        {
            _unitOfWorkMock
                .Setup(m => 
                    m.ArtefactRepository.GetArtefactDbId(
                        It.IsNotNull<IMaintainableObject>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(dataflowExistsInDataDb == DataflowInDataDatabase.Exists ? 1 : -1));
        }

        private void SetupCodeListRepository()
        {
            _unitOfWorkMock
                .Setup(m => 
                    m.CodeListRepository.GetDimensionCodesFromDb(
                        It.IsAny<int>(), 
                        It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(new string[0]));
        }

        private void SetupTransactionRepository(ConcurrentTransactionState concurrentTransactionExists )
        {
            _dotStatDbServiceMock
                .Setup(m => 
                    m.TryNewTransaction(It.IsAny<Transaction>(), _dataflow, 
                    It.IsAny<IMappingStoreDataAccess>(), It.IsAny<CancellationToken>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(concurrentTransactionExists == ConcurrentTransactionState.NotExists))
                .Callback(()=>
                {
                    
                });

            _unitOfWorkMock
                .Setup(m =>
                    m.TransactionRepository.GetInProgressTransactions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<CancellationToken>()))
                .Returns(
                    new List<Transaction> {
                        new Transaction
                        {
                            Id = 1,
                            TableVersion = DbTableVersion.A,
                            RequestedTargetVersion = TargetVersion.Live,
                            FinalTargetVersion = TargetVersion.Live,
                            ExecutionStart = DateTime.Now.AddMinutes(-(MaxDataImportTimeInMinutes + 1))
                        }
                    }.ToAsyncEnumerable());

            _unitOfWorkMock
                .Setup(m =>
                    m.TransactionRepository.GetNextTransactionId(It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(1));

            _unitOfWorkMock
                .Setup(m =>
                    m.TransactionRepository.CreateTransactionItem(
                        It.IsAny<int>(), It.IsAny<string>(),
                        It.IsAny<DotStatPrincipal>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<TransactionType>(),
                        It.IsAny<TargetVersion?>(), It.IsAny<string>(),
                        It.IsAny<CancellationToken>(), It.IsAny<bool>()))
                .Returns(Task.FromResult(
                    new Transaction
                    {
                        Id = 1,
                        TableVersion = DbTableVersion.A,
                        RequestedTargetVersion = TargetVersion.Live,
                        FinalTargetVersion = TargetVersion.Live
                    })
                );
        }

        private void SetupMappingStore(Dsd dsd)
        {
            _mappingStoreDataAccessMock
                .Setup(m => m.GetDsd(
                    ResetDataSpace,
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<bool>(),
                    It.IsAny<ResolveCrossReferences>(),
                    It.IsAny<bool>()
                ))
                .Returns(dsd);
        }

        private void SetupAuthorizationManagement(string allowedDataSpace, PermissionType testedPermission)
        {
            _authManagementMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    allowedDataSpace,
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    testedPermission
                ))
                .Returns(true);

            _authManagementMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    It.IsNotIn(allowedDataSpace),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<PermissionType>()
                ))
                .Returns(false);
        }

        private void SetupAuthorizationManagementTuneManagement(string allowedDataSpace, bool allowed)
        {
            if (allowed)
            {
                var rights = new List<PermissionType>
                {
                    PermissionType.CanDeleteStructuralMetadata,
                    PermissionType.CanUpdateStructuralMetadata,
                    PermissionType.CanImportStructures,
                    PermissionType.CanReadStructuralMetadata,
                };
                foreach (var testedPermission in rights)
                {
                    _authManagementMock
                        .Setup(x => x.IsAuthorized(
                            It.IsAny<DotStatPrincipal>(),
                            allowedDataSpace,
                            It.IsAny<string>(),
                            It.IsAny<string>(),
                            It.IsAny<string>(),
                            testedPermission
                        ))
                        .Returns(true);
                }
            }
            else
            {
                _authManagementMock
                    .Setup(x => x.IsAuthorized(
                        It.IsAny<DotStatPrincipal>(),
                        allowedDataSpace,
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        It.IsAny<PermissionType>()
                    ))
                    .Returns(false);
            }
        }

        private enum DataflowInDataDatabase
        {
            Exists,
            NotExists
        }

        private enum ConcurrentTransactionState
        {
            Exists,
            NotExists
        }
    }
}
