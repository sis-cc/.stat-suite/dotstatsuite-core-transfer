﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Exceptions;
using DotStat.Db.Service;
using DotStat.DB.Repository;
using DotStat.Domain;
using DotStat.Test;
using DotStat.Test.Moq;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Model;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using DotStat.Transfer.Test.Helper;
using DotStat.Transfer.Utils;
using DotStatServices.Transfer;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Controllers;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Moq.Protected;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Exception;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Extensions = DotStat.Transfer.Utils.Extensions;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class ImportControllerTests : UnitTestBase
    {
        private readonly string _dataSpace = "design";
        private Dataflow _dataflow;
        private int _dataObsCount;
        private int _metaObsCount;
        private readonly string _boundary = $"{Guid.NewGuid():N}";
        private Mock<IAuthorizationManagement> _authManagementMock;
        private Mock<IConsumer<TransferParam>> _consumerMock;
        private Mock<BackgroundQueue> _backgroundQueueMock;

        [Test]
        public async Task ImportExcel()
        {
            var eddFile = FormUpload.GetFile("eddFile.xml", "edd/264D_264_SALDI-no-attributes.xml", "application/xml");
            byte[] eddFileStream;
            await using (var ms = new MemoryStream())
            {
                await eddFile.CopyToAsync(ms, CancellationToken);
                eddFileStream = ms.ToArray();
            }

            var excelFile = FormUpload.GetFile("excelFile.xlsx", "excel/264D_264_SALDI.xlsx",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            byte[] excelFileStream;
            await using (var ms = new MemoryStream())
            {
                await excelFile.CopyToAsync(ms, CancellationToken);
                excelFileStream = ms.ToArray();
            }

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"eddFile", new FormUpload.FileParameter(eddFileStream, eddFile.Name, eddFile.ContentType)},
                {"excelFile", new FormUpload.FileParameter(excelFileStream, excelFile.Name, excelFile.ContentType)},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            var controller = GetImportController(false);
            controller.ControllerContext.HttpContext.Request.Body = body;
            controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            var result = await controller.ImportExcel(new ExcelImportParameters()
            {
                dataspace = _dataSpace,
                eddFile = eddFileStream,
                excelFile = excelFileStream,
                lang = "en"
            });

            ImportAssert<ExcelToSqlTransferParam>(_dataflow, result, 100);
        }

        [Test]
        public async Task ValidateImportExcel()
        {
            var eddFile = FormUpload.GetFile("eddFile.xml", "edd/264D_264_SALDI-no-attributes.xml", "application/xml");
            byte[] eddFileStream;
            await using (var ms = new MemoryStream())
            {
                await eddFile.CopyToAsync(ms, CancellationToken);
                eddFileStream = ms.ToArray();
            }

            var excelFile = FormUpload.GetFile("excelFile.xlsx", "excel/264D_264_SALDI.xlsx",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            byte[] excelFileStream;
            await using (var ms = new MemoryStream())
            {
                await excelFile.CopyToAsync(ms, CancellationToken);
                excelFileStream = ms.ToArray();
            }

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"eddFile", new FormUpload.FileParameter(eddFileStream, eddFile.Name, eddFile.ContentType)},
                {"excelFile", new FormUpload.FileParameter(excelFileStream, excelFile.Name, excelFile.ContentType)},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            var controller = GetImportController(false);
            controller.ControllerContext.HttpContext.Request.Body = body;
            controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            var result = await controller.ValidateImportExcel(new ExcelImportParameters
            {
                dataspace = _dataSpace,
                eddFile = eddFileStream,
                excelFile = excelFileStream,
                lang = "en"
            });

            ImportAssert<ExcelToSqlTransferParam>(_dataflow, result, 100);
        }

        [Test]
        public async Task ImportFromPostedSdmxFile()
        {
            var postedFile = FormUpload.GetFile("264D_264_SALDI.csv", "data/264D_264_SALDI.csv", "text/csv");
            byte[] postedFileStream;
            await using (var ms = new MemoryStream())
            {
                await postedFile.CopyToAsync(ms, CancellationToken);
                postedFileStream = ms.ToArray();
            }

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"file", new FormUpload.FileParameter(postedFileStream, postedFile.Name, postedFile.ContentType)},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            var controller = GetImportController(false);
            controller.ControllerContext.HttpContext.Request.Body = body;
            controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            var result = await controller.ImportSdmxFile(new SdmxImportParameters
            {
                dataspace = _dataSpace,
                lang = "en",
                file = postedFileStream
            });

            ImportAssert<SdmxFileToSqlTransferParam>(_dataflow, result, 101, false);
        }

        [Test]
        public async Task ImportMetaFromPostedSdmxFile()
        {
            var postedFile = FormUpload.GetFile("CsvV2_without_obs_value.csv", "data/CsvV2_without_obs_value.csv", "text/csv");
            byte[] postedFileStream;
            await using (var ms = new MemoryStream())
            {
                await postedFile.CopyToAsync(ms, CancellationToken);
                postedFileStream = ms.ToArray();
            }

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"file", new FormUpload.FileParameter(postedFileStream, postedFile.Name, postedFile.ContentType)},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            var controller = GetImportController(true);
            controller.ControllerContext.HttpContext.Request.Body = body;
            controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            var result = await controller.ImportSdmxFile(new SdmxImportParameters
            {
                dataspace = _dataSpace,
                lang = "en",
                file = postedFileStream
            });

            ImportAssert<SdmxFileToSqlTransferParam>(_dataflow, result, 4, true);
        }

        [Test]
        public async Task ValidateImportFromPostedSdmxFile()
        {
            var postedFile = FormUpload.GetFile("264D_264_SALDI.csv", "data/264D_264_SALDI.csv", "text/csv");
            byte[] postedFileStream;
            await using (var ms = new MemoryStream())
            {
                await postedFile.CopyToAsync(ms, CancellationToken);
                postedFileStream = ms.ToArray();
            }

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"file", new FormUpload.FileParameter(postedFileStream, postedFile.Name, postedFile.ContentType)},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            var controller = GetImportController(false);
            controller.ControllerContext.HttpContext.Request.Body = body;
            controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;
            
            var result = await controller.ValidateImportSdmxFile(new SdmxImportParameters()
            {
                dataspace = _dataSpace,
                file = postedFileStream,
                lang = "en"
            });

            ImportAssert<SdmxFileToSqlTransferParam>(_dataflow, result, 101);
        }

        [Test]
        public async Task ImportFromSharedSdmxFile()
        {
            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"filepath", "data/264D_264_SALDI.csv"},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            var controller = GetImportController(false);
            controller.ControllerContext.HttpContext.Request.Body = body;
            controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            var result = await controller.ImportSdmxFile(new SdmxImportParameters
            {
                dataspace = _dataSpace,
                lang = "en",
                filepath = "data/264D_264_SALDI.csv"
            });

            ImportAssert<SdmxFileToSqlTransferParam>(_dataflow, result, 101);
        }

        [Test]
        public async Task ImportFromOkUrl()
        {
            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"filepath", "http://testurl"},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            var controller = GetImportController(false);
            controller.ControllerContext.HttpContext.Request.Body = body;
            controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            Assert.DoesNotThrowAsync(() => controller.ImportSdmxFile(new SdmxImportParameters{
                dataspace=_dataSpace,
                lang= "en",
                filepath= "http://testurl"
            }));
        }

        [Test]
        public async Task ImportFromUrlWithAuth()
        {
            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"filepath", "http://testurl"},
                {"lang", "en"},
                {"authenticateToRemoteURL", "true"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            var controller = GetImportController(false, true);
            controller.ControllerContext.HttpContext.Request.Body = body;
            controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            Assert.DoesNotThrowAsync(() => controller.ImportSdmxFile(new SdmxImportParameters
            {
                dataspace = _dataSpace,
                lang = "en",
                filepath = "http://testurl",
                authenticateToRemoteURL = true
            }));
        }

        [Test]
        public async Task ImportFromUrlHavingBadContent()
        {
            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"filepath", "http://testurl"},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            var controller = GetImportController(false, false, HttpStatusCode.OK, false, true);
            controller.ControllerContext.HttpContext.Request.Body = body;
            controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            Assert.ThrowsAsync<SdmxNotImplementedException>(() => controller.ImportSdmxFile(new SdmxImportParameters
            {
                dataspace = _dataSpace,
                lang = "en",
                filepath = "http://testurl"
            }));
        }

        [Test]
        public async Task ImportFromSlowUrl()
        {
            var controller = GetImportController(false, false, HttpStatusCode.BadGateway, true);

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"filepath", "http://testurl"},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            controller.ControllerContext.HttpContext.Request.Body = body;
            controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            Assert.ThrowsAsync<DotStatException>(() => controller.ImportSdmxFile(new SdmxImportParameters
            {
                dataspace = _dataSpace,
                lang = "en",
                filepath = "http://testurl"
            }));
        }

        [Test]
        [TestCase(HttpStatusCode.OK, true)]
        [TestCase(HttpStatusCode.PartialContent, true)]
        [TestCase(HttpStatusCode.Unauthorized, true)]
        [TestCase(HttpStatusCode.Forbidden, true)]
        [TestCase(HttpStatusCode.NotFound, true)]
        [TestCase(HttpStatusCode.BadGateway, true)]
        [TestCase(HttpStatusCode.ServiceUnavailable, true)]
        [TestCase(HttpStatusCode.InternalServerError, true)]
        [TestCase(HttpStatusCode.OK, false)]
        [TestCase(HttpStatusCode.PartialContent, false)]
        [TestCase(HttpStatusCode.Unauthorized, false)]
        [TestCase(HttpStatusCode.Forbidden, false)]
        [TestCase(HttpStatusCode.NotFound, false)]
        [TestCase(HttpStatusCode.BadGateway, false)]
        [TestCase(HttpStatusCode.ServiceUnavailable, false)]
        [TestCase(HttpStatusCode.InternalServerError, false)]
        public async Task ImportFromUrl(HttpStatusCode httpStatusCode, bool isMetadataOnly)
        {
            var controller = GetImportController(isMetadataOnly, false, httpStatusCode);

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"filepath", "http://testurl"},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            controller.ControllerContext.HttpContext.Request.Body = body;
            controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            if (httpStatusCode > (HttpStatusCode)299)
            {
                Assert.ThrowsAsync<DotStatException>(() => controller.ImportSdmxFile(new SdmxImportParameters
                {
                    dataspace = _dataSpace,
                    lang = "en",
                    filepath = "http://testurl"
                }));
            }
            else
            {
                Assert.DoesNotThrowAsync(() => controller.ImportSdmxFile(new SdmxImportParameters
                {
                    dataspace = _dataSpace,
                    lang = "en",
                    filepath = "http://testurl"
                }));
            }
        }

        [Test]
        public async Task CorrectlyCloseImportFileTest()
        {
            var mappingStore = new TestMappingStoreDataAccess();
            var tempFileManager = new Mock<TempFileManager>(Configuration, new FormOptions());
            _dataflow = mappingStore.GetDataflow();

            var authManagementMock = new Mock<IAuthorizationManagement>();
            authManagementMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    _dataSpace,
                    _dataflow.AgencyId,
                    _dataflow.Code,
                    _dataflow.Version.ToString(),
                    PermissionType.CanImportData
                ))
                .Returns(true);

            var producer = new SdmxFileProducer(authManagementMock.Object, mappingStore, tempFileManager.Object);
            
            // ---------------

            var tempFile = Path.Combine(Path.GetTempPath(), DateTime.Now.ToString("ddMMyyyyHHssffffff"));
            
            File.Copy("data/264D_264_SALDI.csv", tempFile);

            // ---------------

            var param = new SdmxFileToSqlTransferParam(
                new InternalSdmxParameters()
                {
                    filepath = tempFile,
                    dataflow = _dataflow.FullId,
                    dataspace = _dataSpace
                }, 
                new DataspaceInternal() { Id = _dataSpace }, 
                "en"
            );

            var content = await producer.Process(param, _dataflow, CancellationToken.None);

            Assert.IsNotNull(content);

            var enumerator = content.DataObservations.GetAsyncEnumerator();
            var index = 0;

            while (await enumerator.MoveNextAsync())
            {
                if (index++ > 10)
                {
                    break;
                }
            }

            await enumerator.DisposeAsync();

            // ---------------

            Assert.IsTrue(File.Exists(tempFile));
            File.Delete(tempFile);
            Assert.IsFalse(File.Exists(tempFile));
        }

        private void ImportAssert<T>(IDotStatMaintainable dataflow, ActionResult<OperationResult> result, int expectedReadObsCount, bool isMetadata = false) where T : TransferParam
        {
            // Authorization method is called
            //_authManagementMock.Verify(mock => mock.IsAuthorized(
            //        It.IsAny<DotStatPrincipal>(),
            //        _dataSpace,
            //        dataflow.AgencyId,
            //        dataflow.Code,
            //        dataflow.Version.ToString(),
            //        PermissionType.CanImportData
            //    ),
            //    Times.AtLeastOnce
            //);

            // Background task is executed
            _backgroundQueueMock.Verify(mock => mock.Enqueue(It.IsAny<TransactionQueueItem>()), Times.Once);

            _consumerMock.Verify(mock => mock.Save(
                        It.IsAny<T>(),
                        It.IsAny<Transaction>(),
                        It.IsAny<IImportReferenceableStructure>(),
                        It.IsAny<TransferContent>(),
                        It.IsAny<CancellationToken>()
                    ),
                    Times.AtMost(2));

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
            Assert.AreEqual(expectedReadObsCount, isMetadata ? _metaObsCount : _dataObsCount);
        }

        private ImportController GetImportController(bool metadataOnly, bool withAuth = false, HttpStatusCode httpStatusCode = HttpStatusCode.OK, bool getHttpRequestException = false, bool getWrongContent = false)
        {
            Extensions.AllowedImportSchemes.Add(Uri.UriSchemeFile);

            CancellationToken = new CancellationToken();
            this.Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = _dataSpace}
            };

            var authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            var mappingStore = new TestMappingStoreDataAccess(metadataOnly ? "sdmx/CsvV2.xml" : "sdmx/264D_264_SALDI+2.1.xml");
            _dataflow = mappingStore.GetDataflow();

            var contextAccessorMock = new Mock<IHttpContextAccessor>();
            contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _authManagementMock = new Mock<IAuthorizationManagement>();
            _authManagementMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    _dataSpace,
                    _dataflow.AgencyId,
                    _dataflow.Code,
                    _dataflow.Version.ToString(),
                    PermissionType.CanImportData
                ))
                .Returns(true);

            _consumerMock = new Mock<IConsumer<TransferParam>>();
            _consumerMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<TransferParam>(),
                    It.IsAny<IImportReferenceableStructure>()
                ))
                .Returns(true);

            _consumerMock
                .Setup(x => x.Save(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Transaction>(),
                    It.IsAny<IImportReferenceableStructure>(),
                    It.IsAny<TransferContent>(),
                    It.IsAny<CancellationToken>()
                ))
                .Returns(Task.FromResult(true))
                .Callback((TransferParam p, Transaction t, IImportReferenceableStructure d, TransferContent c, CancellationToken ct) =>
                {
                    _dataObsCount = metadataOnly ? 0 : c.DataObservations.CountAsync(cancellationToken: ct).Result;
                    _metaObsCount = metadataOnly ? c.MetadataObservations.CountAsync(cancellationToken: ct).Result : 0;
                });

            _backgroundQueueMock = new Mock<BackgroundQueue>();
            _backgroundQueueMock
                .Setup(x => x.Enqueue(It.IsAny<TransactionQueueItem>()))
                .Callback((TransactionQueueItem transactionQueueItem) =>
                {
                    transactionQueueItem.MainTask.Invoke(new CancellationToken());
                });

            var mockFactory = new Mock<IHttpClientFactory>();
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();

            if (getHttpRequestException)
            {
                mockHttpMessageHandler.Protected()
                    .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                    .Callback(() => throw new HttpRequestException());
            }
            else
            {
                mockHttpMessageHandler.Protected()
                    .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                    .ReturnsAsync(new HttpResponseMessage
                    {
                        StatusCode = httpStatusCode,
                        Content = new StringContent(getWrongContent ? "wrongContent" : File.ReadAllText(metadataOnly ? "data/CsvV2_without_obs_value.csv" : "data/264D_264_SALDI_compact.xml"))
                    });
            }

            var client = new HttpClient(mockHttpMessageHandler.Object);
            mockFactory.Setup(_ => _.CreateClient(It.IsAny<string>())).Returns(client);

            var tempFileManager = new Mock<TempFileManager>(Configuration, new FormOptions());
            var excelToSqlTransferManager = new ExcelToSqlTransferManager(Configuration, new ExcelProducer(mappingStore), _consumerMock.Object);
            var sdmxFileToSqlTransferManager = new SdmxFileToSqlTransferManager(Configuration, new SdmxFileProducer(_authManagementMock.Object, mappingStore, tempFileManager.Object), _consumerMock.Object);
            var urlToSqlTransferManager = new SdmxUrlToSqlTransferManager(Configuration, new SdmxUrlProducer(_authManagementMock.Object, mappingStore, mockFactory.Object, Configuration, tempFileManager.Object), _consumerMock.Object);

            var serviceProvider = ConfigureServices(authConfig);
            var options = serviceProvider.GetService<IOptions<MvcOptions>>();
            var metadataProvider = new EmptyModelMetadataProvider();
            var modelBinderFactory = new ModelBinderFactory(metadataProvider, options, serviceProvider);

            var contentType = $"multipart/form-data; boundary={_boundary}";
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers.Add("Content-Type", contentType);
            if (withAuth)
            {
                httpContext.Request.Headers.Add("Authorization", "Bearer eyJhbGciOiJSUzI1");
            }
            httpContext.Request.ContentType = contentType;
            httpContext.RequestServices = serviceProvider;

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var dotStatDbServiceMock = new Mock<IDotStatDbService>();

            UnitOfWorkResolver unitOfWorkResolver = dataSpaceId => unitOfWorkMock.Object;
            DotStatDbServiceResolver dotStatDbServiceResolver = dataSpaceId => dotStatDbServiceMock.Object;

            unitOfWorkMock.Setup(x => x.TransactionRepository.GetNextTransactionId(It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(0));

            var mailServiceMock = new Mock<IMailService>();


            return new ImportController(
                contextAccessorMock.Object,
                _authManagementMock.Object,
                excelToSqlTransferManager,
                sdmxFileToSqlTransferManager,
                urlToSqlTransferManager,
                Configuration,
                authConfig,
                mailServiceMock.Object,
                _backgroundQueueMock.Object,
                tempFileManager.Object,
                unitOfWorkResolver,
                dotStatDbServiceResolver,
                mappingStore
            )
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                    ValueProviderFactories = new List<IValueProviderFactory> { Mock.Of<IValueProviderFactory>() },
                },
                MetadataProvider = metadataProvider,
                ModelBinderFactory = modelBinderFactory
            };
        }

        private ServiceProvider ConfigureServices(AuthConfiguration authConfig)
        {
            var services = new ServiceCollection();
            services.AddMvc(options =>
            {
                if (authConfig?.Enabled == true)
                {
                    options.Filters.Add(new AuthorizeFilter());
                }
                options.Filters.Add(new ProducesAttribute("application/json"));
                options.Filters.Add(new ProducesResponseTypeAttribute(typeof(OperationResult), (int)HttpStatusCode.OK));
                options.Filters.Add(new ProducesResponseTypeAttribute(typeof(OperationResult), (int)HttpStatusCode.BadRequest));
                options.Filters.Add(new ProducesResponseTypeAttribute((int)HttpStatusCode.Unauthorized));
                options.Filters.Add(new ProducesResponseTypeAttribute((int)HttpStatusCode.Forbidden));
            });

            services.AddLogging(config =>
            {
                config.AddDebug();
                config.AddConsole();
            });

            return services.BuildServiceProvider();
        }
    }
}
