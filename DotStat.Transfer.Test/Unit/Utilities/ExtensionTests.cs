﻿using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Test;
using DotStat.Test.Moq;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Utils;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Util.Io;
using System;
using System.IO;
using Moq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Dataflow = DotStat.Domain.Dataflow;
using Org.Sdmxsource.Sdmx.Api.Model.Query;
using DotStat.Common.Configuration.Dto;
using DotStat.Transfer.Param;
using OfficeOpenXml.Utils;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Transfer.Test.Unit.Utilities
{
    [TestFixture]
    public class ExtensionTests : UnitTestBase
    {
        private Dataflow _dataflow;
        public ExtensionTests()
        {
            var mappingStore = new TestMappingStoreDataAccess("sdmx/OECD-DF_TEST_DELETE-1.0-all_structures.xml");
            _dataflow = mappingStore.GetDataflow();
        }

        [TestCase("", true)]
        [TestCase(null, true)]
        [TestCase("2022-06-04T10:16:01", true)]
        [TestCase("2022-06-04T08:16:01Z", true)]
        [TestCase("2022-06-04T09:46:01+01:30", true)]
        [TestCase("2022-06-04T10:16:01.123", true)]
        [TestCase("2022-06-04T08:16:01.123Z", true)]
        [TestCase("2022-06-04T09:46:01.123+01:30", true)]
        [TestCase("2022-06-03T20:16:01-14:00", true)]
        [TestCase("2022-06-03T20:16:01-1400", false)]
        [TestCase("2022-06-04T24:00:00", false)] // DateTime does not support 24th hour in accordance with ISO ISO 8601-1:2019: it is explicitly disallowed by the 2019 revision.
        [TestCase("2022-06-04T10:16:0", false)]
        [TestCase("2022-06-04T09:46:01.1230+01:30", false)]
        [TestCase("2022-06-04T09:46:01.+01:30", false)]
        [TestCase("06-24-2021 23:22:21", false)]
        [TestCase("24-06-2021 23:22:21", false)]
        [TestCase("06/24/2021 23:22:21", false)]
        [TestCase("24/06/2021 23:22:21", false)]
        [TestCase("2021-06-24T25:22:21", false)]
        [TestCase("0900-06-24T23:22:21", false)]
        [TestCase("900-06-24T23:22:21", false)]
        [TestCase("2021-16-24T23:22:21", false)]
        [TestCase("2021-02-31T23:22:21", false)]
        [TestCase("2021-06-24T25:22:21", false)]
        [TestCase("2021-06-24T23:60:21", false)]
        [TestCase("2021-06-24T23:22:60", false)]
        public void GetPitReleaseDateTest(string dateTimeStr, bool isValidExpected)
        {
            DateTime? dateTimeValue = null;
            if (isValidExpected)
            {
                Assert.DoesNotThrow(() => dateTimeValue = dateTimeStr.GetPITReleaseDate("en"));

                if (string.IsNullOrEmpty(dateTimeStr))
                {
                    Assert.IsNull(dateTimeValue);
                }
                else
                {
                    Assert.IsNotNull(dateTimeValue);
                }
            }
            else
            {
                Assert.Throws<DotStatException>(() => dateTimeValue = dateTimeStr.GetPITReleaseDate("en"));
            }
        }


        [TestCase("", true, "")]
        [TestCase(null, true, "")]
        [TestCase("2022-06-04T10:16:01", true, "2022-06-04T10:16:01Z")]
        [TestCase("2022-06-04 10:16:01", true, "2022-06-04T10:16:01Z")]
        [TestCase("2022-06-04T08:16:01Z", true, "2022-06-04T08:16:01Z", true)]
        [TestCase("2022-06-04T09:46:01+01:30", true, "2022-06-04T08:16:01Z", true)]
        [TestCase("2022-06-04T10:16:01.123", true, "2022-06-04T10:16:01.123Z")]
        [TestCase("2022-06-04T08:16:01.123Z", true, "2022-06-04T08:16:01.123Z", true)]
        [TestCase("2022-06-04T09:46:01.123+01:30", true, "2022-06-04T08:16:01.123Z", true)]
        [TestCase("2022-06-03T20:16:01-14:00", true, "2022-06-04T10:16:01Z", true)]
        [TestCase("2022-06-03T20:16:01-1400", false, "")]
        [TestCase("2022-06-04T24:00:00", false, "")] // DateTime does not support 24th hour in accordance with ISO ISO 8601-1:2019: it is explicitly disallowed by the 2019 revision.
        [TestCase("2022-06-04T10:16:0", false, "")]
        [TestCase("2022-06-04T09:46:01.1230+01:30", false, "")]
        [TestCase("2022-06-04T09:46:01.+01:30", false, "")]
        [TestCase("06-24-2021 23:22:21", false, "")]
        [TestCase("06/24/2021 23:22:21", false, "")]
        [TestCase("24/06/2021 23:22:21", false, "")]
        [TestCase("2021-06-24T25:22:21", false, "")]
        [TestCase("0900-06-24T23:22:21", false, "")]
        [TestCase("900-06-24T23:22:21", false, "")]
        [TestCase("2021-16-24T23:22:21", false, "")]
        [TestCase("2021-02-31T23:22:21", false, "")]
        [TestCase("2021-06-24T25:22:21", false, "")]
        [TestCase("2021-06-24T23:60:21", false, "")]
        [TestCase("2021-06-24T23:22:60", false, "")]
        //Backwards compatible with DLM logbook format
        [TestCase("24-06-2021 23:22:21", true, "2021-06-24T23:22:21.000Z")]

        //Ensure that status datetime filter parameters (submissionStart and submissionEnd) uses time zone info when given 
        //Otherwise server's time zone is assumed
        public void GetDateTimeInUtcTest(string dateTimeStr, bool isValidExpected, string expectedUtcDateTimeStr, bool hasTimeZone = false)
        {
            DateTime? dateTimeValue = null;
            if (isValidExpected)
            {
                Assert.DoesNotThrow(() => dateTimeValue = dateTimeStr.GetDateTimeInUtc("name", "en"));

                if (string.IsNullOrEmpty(dateTimeStr))
                {
                    Assert.IsNull(dateTimeValue);
                    return;
                }

                var expectedUtcDateTime = DateTimeOffset.Parse(expectedUtcDateTimeStr).DateTime;
                Assert.AreEqual(!hasTimeZone ? expectedUtcDateTime.ToUniversalTime() : expectedUtcDateTime,
                    dateTimeValue);
            }
            else
            {
                Assert.Throws<DotStatException>(() => dateTimeValue = dateTimeStr.GetDateTimeInUtc("name", "en"));
            }
        }

        [TestCase("I", StagingRowActionEnum.Merge)]
        [TestCase("A", StagingRowActionEnum.Merge)]
        [TestCase("M", StagingRowActionEnum.Merge)]
        [TestCase("D", StagingRowActionEnum.Delete)]
        [TestCase("R", StagingRowActionEnum.Replace)]
        public void GetStagingRowActionEnum(string action, StagingRowActionEnum? expectedAction)
        {
            var dataReaderEngine = new Mock<IDataReaderEngine>();
            dataReaderEngine.Setup(x => x.CurrentAction).Returns(DatasetAction.GetAction(action));

            Assert.AreEqual(expectedAction, dataReaderEngine.Object.GetStagingRowActionEnum());
        }
              

        //CSV V1
        [TestCase("data/264D_264_SALDI.csv", false)]
        //CSV V2
        [TestCase("data/delete/case_1__delete_whole_content_of_the_dataflow.csv", false)]
        //XML
        [TestCase("data/264D_264_SALDI_generic.xml", true)]
        public void CheckIsXml(string filePath, bool expectedValue)
        {
            using var sourceData = new ReadableDataLocationFactory().GetReadableDataLocation(new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            using var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, _dataflow.Dsd.Base, null, _dataflow.Dsd.Msd?.Base);
            Assert.AreEqual(expectedValue, dataReaderEngine.IsXml());
        }

        //xml
        [TestCase(StagingRowActionEnum.Merge, false, false, true, false)]
        [TestCase(StagingRowActionEnum.Merge, true, true, true, true)]
        [TestCase(StagingRowActionEnum.Delete, false, false, true, false)]
        [TestCase(StagingRowActionEnum.Delete, false, true, true, true)]
        //csv
        [TestCase(StagingRowActionEnum.Merge, false, false, false, false)]
        [TestCase( StagingRowActionEnum.Merge, true, true, false, true)]
        [TestCase(StagingRowActionEnum.Delete, false, false, false, false)]
        [TestCase(StagingRowActionEnum.Delete, false, true, false, true)]
        //non supported actions
        [TestCase(StagingRowActionEnum.Skip, true, true, true, false, false)]
        [TestCase(StagingRowActionEnum.DeleteAll, true, true, true, false, false)]
        public void CheckAuthorized(StagingRowActionEnum action, bool canMergeData, bool canDeleteData, bool isXml, bool isAuthorized, bool isSupported = true)
        {
            var unauthorizedToMergeText = isXml
                ? string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToMergeXml), 1)
                : string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToMergeCsv), 1, 1 + 1);

            var unauthorizedToDeleteText = isXml
                ? string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToDeleteXml), 1)
                : string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToDeleteCsv), 1, 1 + 1);

            if (isAuthorized)
                Assert.DoesNotThrow(() =>
                    action.CheckAuthorized(canMergeData, canDeleteData, canMergeData, 1, isXml));
            else if (!isSupported)
            {
                Assert.Throws<NotSupportedException>(() =>
                    action.CheckAuthorized(canMergeData, canDeleteData, canMergeData, 1, isXml));
            }
            else
            {
                var ex = Assert.Throws<TransferUnauthorizedException>(() =>
                    action.CheckAuthorized(canMergeData, canDeleteData, canMergeData, 1, isXml));

                Assert.AreEqual(
                    action is StagingRowActionEnum.Delete ? unauthorizedToDeleteText : unauthorizedToMergeText,
                    ex.Message);
            }
        }


        [TestCase("", true)]
        [TestCase(null, true)]
        [TestCase("D.NOK.EUR.SP00.A?startPeriod=2012-01-01&endPeriod=2010-02-01", true)]
        [TestCase("D.NOK.EUR.SP00.A?startPeriod=2012-01-01&endPeriod=2010-02-01&UpdatedAfter=2023", true)]
        [TestCase("all?UpdatedAfter=2022-06-04", true)]
        [TestCase("all?UpdatedAfter=2021-06-24T23:22:60", false)]
        [TestCase("all?UpdatedAfter=2021-06-24T23:22:00", true)]
        [TestCase("all?UpdatedAfter=24/06/2021 23:22:21", false)]
        [TestCase("all?UpdatedAfter=2022-06-04T09:46:01+01:30", true)]
        [TestCase("all?UpdatedAfter=2022-06-04T09:46:01.123+01:30", true)]
        [TestCase("all?UpdatedAfter=2022-06-03T20:16:01-14:00", true)]
        public void GetRestDataQuery(string sourceQuery, bool isValidExpected)
        {
            IRestDataQuery restDataQuery = null;
            var df = "AGENCY:ID:1.0".GetDataflow("en");
            if (isValidExpected)
            {
                Assert.DoesNotThrow(() => restDataQuery = sourceQuery.GetRestDataQuery(new IdentifiableReferencedStructure { AgencyId = df.AgencyId, Id = df.Id, Version = df.Version.ToString() }, "en"));
                Assert.IsNotNull(restDataQuery.RestQuery);
            }
            else
            {
                Assert.Throws<Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException>(() => restDataQuery = sourceQuery.GetRestDataQuery(new IdentifiableReferencedStructure { AgencyId = df.AgencyId, Id = df.Id, Version = df.Version.ToString() }, "en"));
            }
        }

        [TestCase("all?UpdatedAfter=2022-06-04T10:16:01", "2022-06-04T10:16:01Z")]
        [TestCase("all?UpdatedAfter=2022-06-04T08:16:01Z", "2022-06-04T08:16:01Z")]
        [TestCase("all?UpdatedAfter=2022-06-04T09:46:01+01:30", "2022-06-04T08:16:01Z", true)]
        [TestCase("all?UpdatedAfter=2022-06-04T08:16:01.123Z", "2022-06-04T08:16:01.123Z")]
        [TestCase("all?UpdatedAfter=2022-06-04T09:46:01.123+01:30", "2022-06-04T08:16:01.123Z", true)]
        [TestCase("all?UpdatedAfter=2022-06-03T20:16:01-14:00", "2022-06-04T10:16:01Z", true)]
        [TestCase("all?UpdatedAfter=2022-11-04T10:16:01", "2022-11-04T10:16:01Z")]
        [TestCase("all?UpdatedAfter=2022-11-04T08:16:01Z", "2022-11-04T08:16:01Z")]
        [TestCase("all?UpdatedAfter=2022-11-04T09:46:01+01:30", "2022-11-04T08:16:01Z", true)]
        [TestCase("all?UpdatedAfter=2022-11-04T08:16:01.123Z", "2022-11-04T08:16:01.123Z")]
        [TestCase("all?UpdatedAfter=2022-11-04T09:46:01.123+01:30", "2022-11-04T08:16:01.123Z", true)]
        [TestCase("all?UpdatedAfter=2022-11-03T20:16:01-14:00", "2022-11-04T10:16:01Z", true)]

        //Ensure that IDataQuery.UpdatedAfter attribute uses time zone info when given by the sourceQuery's updatedAfter param 
        //Otherwise server's time zone is assumed
        public void LastUpdatedUtcDataQueryTest(string sourceQuery, string expectedUtcDateTimeStr, bool hasTimeZone = false)
        {
            var df = "AGENCY:ID:1.0".GetDataflow("en");
            var restDataQuery = sourceQuery.GetRestDataQuery(new IdentifiableReferencedStructure { AgencyId = df.AgencyId, Id = df.Id, Version = df.Version.ToString() }, "en");

            var expectedUtcDateTime = DateTimeOffset.Parse(expectedUtcDateTimeStr).DateTime;
            if (!hasTimeZone)
            {
                //Add the time offset of local time zone at the time of the expected date (note diff. between summer and winter times)
                var offset = TimeZoneInfo.Local.GetUtcOffset(expectedUtcDateTime);
                Assert.AreEqual(expectedUtcDateTime.Add(offset), restDataQuery.UpdatedAfter.Date);
            }
            else
            {
                Assert.AreEqual(expectedUtcDateTime, restDataQuery.UpdatedAfter.Date);
            }
        }

        [TestCase(true)]
        [TestCase(false)]
        public void DataSpaceCompuression(bool archive)
        {
            var dataspace = new DataspaceInternal() { Archive = archive };
            var expectedCompression = archive ? DataCompressionEnum.COLUMNSTORE_ARCHIVE : DataCompressionEnum.NONE;
            Assert.AreEqual(expectedCompression, dataspace.GetDataCompression());
        }

        [TestCase(SendEmailOptionsEnum.Never, TransactionStatus.Completed, true, false)]
        [TestCase(SendEmailOptionsEnum.Never, TransactionStatus.Completed, false, false)]
        [TestCase(SendEmailOptionsEnum.Never, TransactionStatus.Canceled, true, false)]
        [TestCase(SendEmailOptionsEnum.Never, TransactionStatus.Canceled, false, false)]
        [TestCase(SendEmailOptionsEnum.Never, TransactionStatus.Unknown, true, false)]
        [TestCase(SendEmailOptionsEnum.Never, TransactionStatus.Unknown, false, false)]
        [TestCase(SendEmailOptionsEnum.Never, TransactionStatus.InProgress, true, false)]
        [TestCase(SendEmailOptionsEnum.Never, TransactionStatus.InProgress, false, false)]
        [TestCase(SendEmailOptionsEnum.Never, TransactionStatus.Queued, true, false)]
        [TestCase(SendEmailOptionsEnum.Never, TransactionStatus.Queued, false, false)]
        [TestCase(SendEmailOptionsEnum.Always, TransactionStatus.Completed, true, true)]
        [TestCase(SendEmailOptionsEnum.Always, TransactionStatus.Completed, false, true)]
        [TestCase(SendEmailOptionsEnum.Always, TransactionStatus.Canceled, true, true)]
        [TestCase(SendEmailOptionsEnum.Always, TransactionStatus.Canceled, false, true)]
        [TestCase(SendEmailOptionsEnum.Always, TransactionStatus.Unknown, true, true)]
        [TestCase(SendEmailOptionsEnum.Always, TransactionStatus.Unknown, false, true)]
        [TestCase(SendEmailOptionsEnum.Always, TransactionStatus.InProgress, true, true)]
        [TestCase(SendEmailOptionsEnum.Always, TransactionStatus.InProgress, false, true)]
        [TestCase(SendEmailOptionsEnum.Always, TransactionStatus.Queued, true, true)]
        [TestCase(SendEmailOptionsEnum.Always, TransactionStatus.Queued, false, true)]
        [TestCase(SendEmailOptionsEnum.IfError, TransactionStatus.Completed, true, true)]
        [TestCase(SendEmailOptionsEnum.IfError, TransactionStatus.Completed, false, false)]
        [TestCase(SendEmailOptionsEnum.IfError, TransactionStatus.Canceled, true, true)]
        [TestCase(SendEmailOptionsEnum.IfError, TransactionStatus.Canceled, false, true)]
        [TestCase(SendEmailOptionsEnum.IfError, TransactionStatus.Unknown, true, true)]
        [TestCase(SendEmailOptionsEnum.IfError, TransactionStatus.Unknown, false, false)]
        [TestCase(SendEmailOptionsEnum.IfError, TransactionStatus.InProgress, true, true)]
        [TestCase(SendEmailOptionsEnum.IfError, TransactionStatus.InProgress, false, false)]
        [TestCase(SendEmailOptionsEnum.IfError, TransactionStatus.Queued, true, true)]
        [TestCase(SendEmailOptionsEnum.IfError, TransactionStatus.Queued, false, false)]
        public void SendEmailOn(SendEmailOptionsEnum sendEmailOption, TransactionStatus transactionStatus, bool hasErrors, bool sendEmail)
        {
            Assert.AreEqual(sendEmail, sendEmailOption.SendEmail(transactionStatus, hasErrors));

        }

        [Test, Explicit]
        public void ReplaceIds()
        {
            var dsd_id = 2256;
            var new_dsd_id = 1;

            var comp_id = 2379;
            var new_comp_id = 1;

            var original = File.ReadAllText(@"D:\Work\_OECD\crs\queries.sql");

            var replacement = original.Replace(dsd_id.ToString(), new_dsd_id.ToString());

            for (var i = 0; i < 50; i++)
            {
                replacement = replacement.Replace(comp_id.ToString(), new_comp_id.ToString());

                comp_id++;
                new_comp_id++;
            }

            File.WriteAllText(@"D:\Work\_OECD\crs\queries_remote.sql", replacement);


        }
    }
}
